ext
===

This directory holds git submodules that point to libraries needed by the OpenEXR plug-ins.

You will need to manually add the After Effects and Photoshop SDKs, available from [Adobe](https://console.adobe.io/downloads).

At some point Adobe stopped labeling their SDKs with the version of the app they belong to, but we still use it.  So you'll have to rename "AfterEffectsSDK" to "Adobe After Effects CC 2019 Mac SDK" or "Adobe After Effects CC 2019 Win SDK".  For Photoshop, rename "photoshopsdk" to "adobe_photoshop_cc_2019_sdk_mac" or "adobe_photoshop_cc_2019_sdk_win".


If the submodule contents are missing, you should be able to get them by typing:

`git submodule init`
`git submodule update`
