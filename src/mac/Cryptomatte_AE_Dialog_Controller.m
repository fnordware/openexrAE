//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import "Cryptomatte_AE_Dialog_Controller.h"

#import "Cryptomatte_AE_Wildcards_Obj.h"

@implementation Cryptomatte_AE_Dialog_Controller

#ifndef __llvm__
@synthesize layerMenu;
@synthesize selectionField;
@synthesize manifestField;
@synthesize currentLayer;
#endif

- (id)init:(NSString *)layer
	selection:(NSString *)selection
	manifest:(NSString *)manifest
	layers:(NSSet *)layers
{
	self = [super initWithWindowNibName:@"Cryptomatte_AE_Dialog"];
	
	if(self)
	{
		if(!([NSBundle loadNibNamed:@"Cryptomatte_AE_Dialog" owner:self]))
			return nil;
		
		[[self window] center];
		
		[layers enumerateObjectsUsingBlock:
			^(id obj, BOOL *stop)
			{
				NSAssert([obj isKindOfClass:[NSString class]], @"Expected NSString");
				
				NSString *layerName = obj;
				
				[self.layerMenu addItemWithTitle:layerName];
			}];
		
		[self.layerMenu selectItemWithTitle:layer];
		
		[self.selectionField setStringValue:selection];
		[self.manifestField setStringValue:manifest];
		
		self.currentLayer = layer;
		warnedAboutChangingLayers = NO;
	}
	
	return self;
}

- (IBAction)clickOK:(NSButton *)sender {
    [NSApp stopModal];
}

- (IBAction)clickCancel:(NSButton *)sender {
    [NSApp abortModal];
}

- (IBAction)onLayerChange:(NSPopUpButton *)sender {
	if(!warnedAboutChangingLayers && ![self.currentLayer isEqualToString:[self.layerMenu titleOfSelectedItem]] &&
		(![[self.selectionField stringValue] isEqualToString:@""] || ![[self.manifestField stringValue] isEqualToString:@""]))
	{
		NSAlert *alert = [NSAlert alertWithMessageText:@"Changing layers will probably invalidate the current selection and manifest."
							defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@""];
		
		NSBeep();
		
		[alert runModal];
		
		warnedAboutChangingLayers = YES;
	}
	
	self.currentLayer = [self.layerMenu titleOfSelectedItem];
}

- (IBAction)clickExpand:(NSButton *)sender {
	if([[self getSelection] length] > 0 && [[self getManifest] length] > 0)
	{
		NSString *newSelection = [Cryptomatte_AE_Wildcards_Obj expand:[self.selectionField stringValue] withManifest:[self.manifestField stringValue]];
		
		if(newSelection != nil)
			[self.selectionField setStringValue:newSelection];
	}
	else
	{
		NSAlert *alert = [NSAlert alertWithMessageText:@"Wildcard failure"
									defaultButton:nil alternateButton:nil otherButton:nil
									informativeTextWithFormat:@"Wildcard expansion requires selection text with wildcards (* or ?) and a JSON manifest."];
		
		NSBeep();
		
		[alert runModal];
	}
}

- (NSString *)getLayer {
	return ([self.layerMenu titleOfSelectedItem] != nil ? [self.layerMenu titleOfSelectedItem] : @"");
}

- (NSString *)getSelection {
	return [self.selectionField stringValue];
}

- (NSString *)getManifest {
	return [self.manifestField stringValue];
}

@end
