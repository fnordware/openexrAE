#import <Cocoa/Cocoa.h>

#if !NSINTEGER_DEFINED
typedef int NSInteger;
#define NSINTEGER_DEFINED 1
#endif


@interface IDentifier_Dialog_Controller : NSObject
{
#if !__LP64__
	NSPopUpButton *idMenu;
	NSWindow *window;
#endif
}

@property (assign) IBOutlet IBOutlet NSPopUpButton *idMenu;
@property (assign) IBOutlet IBOutlet NSWindow *window;


- (IBAction)clickedOK:(NSButton *)sender;
- (IBAction)clickedCancel:(NSButton *)sender;

- (NSPopUpButton *)getMenu;
- (NSWindow *)getWindow;
@end
