/* ---------------------------------------------------------------------
// 
// ProEXR - OpenEXR plug-ins for Photoshop and After Effects
// Copyright (c) 2007-2017,  Brendan Bolles, http://www.fnordware.com
// 
// This file is part of ProEXR.
//
// ProEXR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// -------------------------------------------------------------------*/

#import "ProEXR_AE_GUI_Controller.h"

#import <Foundation/NSPathUtilities.h>

@implementation ProEXR_AE_GUI_Controller

#ifndef __llvm__
@synthesize timeSpanMenu;
@synthesize startField;
@synthesize startLabel;
@synthesize endField;
@synthesize endLabel;
@synthesize compressionMenu;
@synthesize floatButton;
@synthesize compositeButton;
@synthesize hiddenButton;
#endif

- (id)init:(NSInteger)compression
	useFloat:(BOOL)floatVal
	composite:(BOOL)compositeVal 
	hidden:(BOOL)hiddenVal
	timeSpan:(DialogTimeSpan)timeSpan
	startFrame:(NSInteger)startFrame
	endFrame:(NSInteger)endFrame
	currentFrame:(NSInteger)currentFrame
	workStart:(NSInteger)workStart
	workEnd:(NSInteger)workEnd
	compStart:(NSInteger)compStart
	compEnd:(NSInteger)compEnd
{
	self = [super init];
	
	if(!([NSBundle loadNibNamed:@"ProEXR_AE_GUI" owner:self]))
		return nil;
	
	if(self)
	{
		[self.compressionMenu removeAllItems];
		[self.compressionMenu addItemsWithTitles:
			[NSArray arrayWithObjects:@"None", @"RLE", @"Zip", @"Zip16", @"Piz", @"PXR24", @"B44", @"B44A", @"DWAA", @"DWAB", nil]];
		[self.compressionMenu selectItem:[self.compressionMenu itemAtIndex:compression]];
	
		[self.floatButton setState:(floatVal ? NSOnState : NSOffState)];
		[self.compositeButton setState:(compositeVal ? NSOnState : NSOffState)];
		[self.hiddenButton setState:(hiddenVal ? NSOnState : NSOffState)];
		
		[self.timeSpanMenu selectItemWithTag:timeSpan];
		
		current_frame = currentFrame;
		work_start = workStart;
		work_end = workEnd;
		comp_start = compStart;
		comp_end = compEnd;
		
		[[self.startField formatter] setMinimum:[NSNumber numberWithInteger:comp_start]];
		[[self.startField formatter] setMaximum:[NSNumber numberWithInteger:comp_end]];
		
		[[self.endField formatter] setMinimum:[NSNumber numberWithInteger:comp_start]];
		[[self.endField formatter] setMaximum:[NSNumber numberWithInteger:comp_end]];
		
		[self trackTimeFrame:nil];
		
		[[self window] center];
	}
	
	return self;
}
	
- (IBAction)clickedRender:(id)sender
{
	[NSApp stopModal];
}

- (IBAction)clickedCancel:(id)sender
{
	[NSApp abortModal];
}

- (IBAction)trackTimeFrame:(id)sender
{
	const DialogTimeSpan timeSpan = [self getTimeSpan];
	
	if(timeSpan == DIALOG_TIMESPAN_CURRENT_FRAME)
	{
		[self.startField setIntegerValue:current_frame];
		[self.endField setIntegerValue:current_frame];
	}
	else if(timeSpan == DIALOG_TIMESPAN_WORK_AREA)
	{
		[self.startField setIntegerValue:work_start];
		[self.endField setIntegerValue:work_end];
	}
	else if(timeSpan == DIALOG_TIMESPAN_FULL_COMP)
	{
		[self.startField setIntegerValue:comp_start];
		[self.endField setIntegerValue:comp_end];
	}
	
	const BOOL fieldsEnabled = (timeSpan == DIALOG_TIMESPAN_CUSTOM);
	
	[self.startField setEnabled:fieldsEnabled];
	[self.endField setEnabled:fieldsEnabled];
}

- (NSInteger)getCompression
{
	return [self.compressionMenu indexOfSelectedItem];
}

- (BOOL)getFloat
{
	return ([self.floatButton state] == NSOnState);
}

- (BOOL)getComposite
{
	return ([self.compositeButton state] == NSOnState);
}

- (BOOL)getHidden
{
	return ([self.hiddenButton state] == NSOnState);
}

- (DialogTimeSpan)getTimeSpan
{
	return [self.timeSpanMenu indexOfSelectedItem];
}

- (NSInteger)getStart
{
	return [self.startField integerValue];
}

- (NSInteger)getEnd
{
	return [self.endField integerValue];
}

@end
