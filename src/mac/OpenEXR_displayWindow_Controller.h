//
//	OpenEXR file importer/exporter for After Effects (AEIO)
// 
//	by Brendan Bolles <brendan@fnordware.com>
//
//	see OpenEXR.cpp for more information
//

#import <Cocoa/Cocoa.h>

#if !NSINTEGER_DEFINED
typedef int NSInteger;
typedef unsigned long NSUInteger;
#define NSINTEGER_DEFINED 1
#endif

typedef enum {
	DWDIALOG_RESULT_CONTINUE = 0,
	DWDIALOG_RESULT_OK,
	DWDIALOG_RESULT_CANCEL
} DWDialogResult;

@interface OpenEXR_displayWindow_Controller : NSObject
{
	BOOL subDialog;
	DWDialogResult theResult;
	
	BOOL displayWindowDefault;

#if !__LP64__
	NSWindow *theWindow;
	NSControl *messageText;
	NSMatrix *displayWindowRadioGroup;
	NSButton *autoShowCheck;
#endif
}

@property (assign) IBOutlet NSWindow *theWindow;
@property (assign) IBOutlet NSControl *messageText;
@property (assign) IBOutlet NSMatrix *displayWindowRadioGroup;
@property (assign) IBOutlet NSButton *autoShowCheck;


- (id)init:(const char *)message
	dispWtxt:(const char *)dispW_message
	dataWtxt:(const char *)dataW_message
	displayWindow:(BOOL)dispW
	autoShow:(BOOL)automaticShow
	subDialog:(BOOL)subD;

- (IBAction)clickOK:(id)sender;
- (IBAction)clickCancel:(id)sender;
- (DWDialogResult)getResult;

- (NSWindow *)getWindow;

- (IBAction)clickSetDefault:(id)sender;

- (BOOL)getDisplayWindow;
- (BOOL)getDisplayWindowDefault;
- (BOOL)getAutoShow;

@end
