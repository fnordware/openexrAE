//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#include "Cryptomatte_AE_Dialog.h"

#import "Cryptomatte_AE_Dialog_Controller.h"
#import "Cryptomatte_AE_Alt_Dialog_Controller.h"

#include "Cryptomatte_AE.h"

#include "picojson.h"

#ifdef __llvm__
	#if __has_feature(objc_arc)
		#define USE_ARC 1
	#endif
#endif

bool Cryptomatte_Dialog(
	std::string					&layer,
	std::string					&selection,
	std::string					&manifest,
	const std::set<std::string>	&layers,
	const char					*plugHndl,
	const void					*mwnd)
{
	bool clicked_ok = false;

#if !USE_ARC
	NSApplicationLoad();
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
#endif
	
	const NSUInteger keys = [NSEvent modifierFlags];
	const BOOL option_key = !!(keys & NSAlternateKeyMask);

	if(option_key && manifest.size() > 0)
	{
		std::vector<std::string> selection_vec;
		CryptomatteContext::quotedTokenize(selection, selection_vec);
		
		NSMutableArray *selection_array = [NSMutableArray arrayWithCapacity:selection_vec.size()];
		
		for(std::vector<std::string>::const_iterator i = selection_vec.begin(); i != selection_vec.end(); ++i)
		{
			const std::string selection_item = CryptomatteContext::deQuote(*i);
			
			[selection_array addObject:[NSString stringWithUTF8String:selection_item.c_str()]];
		}
		
		
		NSMutableArray *manifest_array = [NSMutableArray array];
	
		picojson::value manifestObj;
		picojson::parse(manifestObj, manifest);

		if( manifestObj.is<picojson::object>() )
		{
			const picojson::object &object = manifestObj.get<picojson::object>();
			
			for(picojson::object::const_iterator i = object.begin(); i != object.end(); ++i)
			{
				const std::string &manifest_item = i->first;
				
				[manifest_array addObject:[NSString stringWithUTF8String:manifest_item.c_str()]];
			}
		}
		
		if([manifest_array count] > 0)
		{
			Cryptomatte_AE_Alt_Dialog_Controller *ui_controller = [[Cryptomatte_AE_Alt_Dialog_Controller alloc]
																	init:selection_array
																	manifest:manifest_array];
			if(ui_controller)
			{
				NSWindow *my_window = [ui_controller window];
				
				if(my_window)
				{
					NSInteger result = [NSApp runModalForWindow:my_window];
					
					if(result == NSRunStoppedResponse)
					{
						NSArray *new_selection = [ui_controller getSelection];
						
						std::string new_selection_str;
						
						for(NSString *item in new_selection)
						{
							assert([item isKindOfClass:[NSString class]]);
							
							if(!new_selection_str.empty())
								new_selection_str += ", ";
							
							new_selection_str += CryptomatteContext::enQuoteIfNecessary([item UTF8String]);
						}
						
						selection = new_selection_str;
						
						clicked_ok = true;
					}
					
					[my_window close];
				}
				
			#if !USE_ARC
				[ui_controller release];
			#endif
			}
		}
	}
	else
	{
		NSMutableSet *layerSet = [NSMutableSet set];
		
		for(std::set<std::string>::const_iterator i = layers.begin(); i != layers.end(); ++i)
		{
			[layerSet addObject:[NSString stringWithUTF8String:i->c_str()]];
		}
		
		Cryptomatte_AE_Dialog_Controller *ui_controller = [[Cryptomatte_AE_Dialog_Controller alloc]
															init:[NSString stringWithUTF8String:layer.c_str()]
															selection:[NSString stringWithUTF8String:selection.c_str()]
															manifest:[NSString stringWithUTF8String:manifest.c_str()]
															layers:layerSet];
		if(ui_controller)
		{
			NSWindow *my_window = [ui_controller window];
			
			if(my_window)
			{
				NSInteger result = [NSApp runModalForWindow:my_window];
				
				if(result == NSRunStoppedResponse)
				{
					layer = [[ui_controller getLayer] UTF8String];
					selection = [[ui_controller getSelection] UTF8String];
					manifest = [[ui_controller getManifest] UTF8String];
					
					clicked_ok = true;
				}
				
				[my_window close];
			}
			
		#if !USE_ARC
			[ui_controller release];
		#endif
		}
	}
	
#if !USE_ARC
	if(pool)
		[pool release];
#endif

	return clicked_ok;
}


void SetMickeyCursor()
{
	[[NSCursor pointingHandCursor] set];
}
