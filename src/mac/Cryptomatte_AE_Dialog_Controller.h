//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import <Cocoa/Cocoa.h>

#if !NSINTEGER_DEFINED
typedef int NSInteger;
#define NSINTEGER_DEFINED 1
#endif

@interface Cryptomatte_AE_Dialog_Controller : NSWindowController
{
	BOOL warnedAboutChangingLayers;
}

@property (assign) IBOutlet NSPopUpButton *layerMenu;
@property (assign) IBOutlet NSTextField *selectionField;
@property (assign) IBOutlet NSTextField *manifestField;

@property (retain) NSString *currentLayer;


- (id)init:(NSString *)layer
	selection:(NSString *)selection
	manifest:(NSString *)manifest
	layers:(NSSet *)layers;

- (IBAction)clickOK:(NSButton *)sender;
- (IBAction)clickCancel:(NSButton *)sender;

- (IBAction)onLayerChange:(NSPopUpButton *)sender;

- (IBAction)clickExpand:(NSButton *)sender;

- (NSString *)getLayer;
- (NSString *)getSelection;
- (NSString *)getManifest;

@end

