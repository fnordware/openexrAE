//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import <Cocoa/Cocoa.h>

@interface Cryptomatte_AE_Wildcards_Obj : NSObject
{
}

+ (NSString *)expand:(NSString *)selection withManifest:(NSString *)manifest;

@end

