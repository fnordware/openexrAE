#import <Cocoa/Cocoa.h>

#if !NSINTEGER_DEFINED
typedef int NSInteger;
#define NSINTEGER_DEFINED 1
#endif


@interface EXtractoR_Dialog_Controller : NSObject
{
#if __LP64__
	NSPopUpButton *layerMenu;
	NSPopUpButton *redMenu;
	NSPopUpButton *greenMenu;
	NSPopUpButton *blueMenu;
	NSPopUpButton *alphaMenu;
	NSWindow *window;
	NSDictionary *layers;
#endif
}

@property (assign) IBOutlet NSPopUpButton *layerMenu;
@property (assign) IBOutlet NSPopUpButton *redMenu;
@property (assign) IBOutlet NSPopUpButton *greenMenu;
@property (assign) IBOutlet NSPopUpButton *blueMenu;
@property (assign) IBOutlet NSPopUpButton *alphaMenu;
@property (assign) IBOutlet NSWindow *window;

@property (assign) NSDictionary *layers;

- (id)init:(NSArray *)chans
	layers:(NSDictionary *)lays
	red:(NSString *)r
	green:(NSString *)g
	blue:(NSString *)b
	alpha:(NSString *)a;

- (IBAction)clickOK:(NSButton *)sender;
- (IBAction)clickCancel:(NSButton *)sender;

- (IBAction)trackLayerMenu:(id)sender;

- (NSWindow *)getWindow;

- (NSString *)getRed;
- (NSString *)getGreen;
- (NSString *)getBlue;
- (NSString *)getAlpha;

@end

