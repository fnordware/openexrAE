//
//  ProEXR_AE_Comp_Controller.m
//
//  Created by Brendan Bolles on 7/7/19.
//  Copyright 2019 fnord. All rights reserved.
//

#import "ProEXR_AE_Comp_Controller.h"

@implementation ProEXR_AE_Comp_Controller

#ifndef __llvm__
@synthesize importAsMatrix;
@synthesize preComposeCheck;
@synthesize contactSheetCheck;
@synthesize sequenceCheck;
@synthesize forceAlphabeticalCheck;
#endif

- (id)init:(BOOL)importAsComp
	preCompose:(BOOL)preCompose
	contactSheet:(BOOL)contactSheet
	sequence:(BOOL)sequence
	forceAlphabetical:(BOOL)forceAlphabetical
	hideSequenceOptions:(BOOL)hideSequenceOptions
{
	self = [super init];
	
	if(!([NSBundle loadNibNamed:@"ProEXR_AE_Comp" owner:self]))
		return nil;
	
	if(self)
	{
		[self.importAsMatrix selectCellWithTag:(importAsComp ? 0 : 1)];
		[self.preComposeCheck setState:(preCompose ? NSOnState : NSOffState)];
		[self.contactSheetCheck setState:(contactSheet ? NSOnState : NSOffState)];
		[self.sequenceCheck setState:(sequence ? NSOnState : NSOffState)];
		[self.forceAlphabeticalCheck setState:(forceAlphabetical ? NSOnState : NSOffState)];
		
		[self trackImportAs:nil];
		[self trackSequence:nil];
		
		if(hideSequenceOptions)
		{
			[self.sequenceCheck setEnabled:NO];
			[self.forceAlphabeticalCheck setEnabled:NO];
		}
		
		[[self window] center];
	}
	
	return self;
}

- (IBAction)clickedOK:(id)sender
{
	[NSApp stopModal];
}

- (IBAction)clickedCancel:(id)sender
{
	[NSApp abortModal];
}

- (IBAction)trackImportAs:(id)sender
{
	const BOOL enableOptions = [self getImportAsComp];
	
	[self.preComposeCheck setEnabled:enableOptions];
	[self.contactSheetCheck setEnabled:enableOptions];
}

- (IBAction)trackSequence:(id)sender
{
	[self.forceAlphabeticalCheck setEnabled:([self.sequenceCheck state] == NSOnState)];
}

- (BOOL)getImportAsComp
{
	return ([self.importAsMatrix selectedRow] == 0);
}

- (BOOL)getPreCompose
{
	return ([self.preComposeCheck state] == NSOnState);
}

- (BOOL)getContactSheet
{
	return ([self.contactSheetCheck state] == NSOnState);
}

- (BOOL)getSequence
{
	return ([self.sequenceCheck state] == NSOnState);
}

- (BOOL)getForceAlphabetical
{
	return ([self.forceAlphabeticalCheck state] == NSOnState);
}

@end
