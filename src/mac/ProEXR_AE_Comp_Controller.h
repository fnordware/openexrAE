//
//  ProEXR_AE_Comp_Controller.h
//
//  Created by Brendan Bolles on 7/7/19.
//  Copyright 2019 frord. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ProEXR_AE_Comp_Controller : NSWindowController
{
#if !__LP64__
	NSMatrix *importAsMatrix;
	NSButton *preComposeCheck;
	NSButton *contactSheetCheck;
	NSButton *sequenceCheck;
	NSButton *forceAlphabeticalCheck;
#endif
}

@property (assign) IBOutlet NSMatrix *importAsMatrix;
@property (assign) IBOutlet NSButton *preComposeCheck;
@property (assign) IBOutlet NSButton *contactSheetCheck;
@property (assign) IBOutlet NSButton *sequenceCheck;
@property (assign) IBOutlet NSButton *forceAlphabeticalCheck;

- (id)init:(BOOL)importAsComp
	preCompose:(BOOL)preCompose
	contactSheet:(BOOL)contactSheet
	sequence:(BOOL)sequence
	forceAlphabetical:(BOOL)forceAlphabetical
	hideSequenceOptions:(BOOL)hideSequenceOptions;

- (IBAction)clickedOK:(id)sender;
- (IBAction)clickedCancel:(id)sender;

- (IBAction)trackImportAs:(id)sender;
- (IBAction)trackSequence:(id)sender;

- (BOOL)getImportAsComp;
- (BOOL)getPreCompose;
- (BOOL)getContactSheet;
- (BOOL)getSequence;
- (BOOL)getForceAlphabetical;

@end
