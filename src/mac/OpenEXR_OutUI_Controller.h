#import <Cocoa/Cocoa.h>


#if !NSINTEGER_DEFINED
typedef int NSInteger;
#define NSINTEGER_DEFINED 1
#endif

typedef enum {
	DIALOG_RESULT_CONTINUE = 0,
	DIALOG_RESULT_OK,
	DIALOG_RESULT_CANCEL
} DialogResult;

@interface OpenEXR_OutUI_Controller : NSObject
{
	BOOL subDialog;
	DialogResult theResult;

#if !__LP64__
	NSWindow *theWindow;
	NSPopUpButton *compressionPulldown;
	NSButton *floatCheck;
	NSTextField *floatLabel;
	NSButton *lumiChromCheck;
#endif
}

@property (assign) IBOutlet NSWindow *theWindow;
@property (assign) IBOutlet NSPopUpButton *compressionPulldown;
@property (assign) IBOutlet NSButton *floatCheck;
@property (assign) IBOutlet NSTextField *floatLabel;
@property (assign) IBOutlet NSButton *lumiChromCheck;

- (IBAction)trackLumiChrom:(id)sender;
- (IBAction)clickOK:(id)sender;
- (IBAction)clickCancel:(id)sender;
- (NSWindow *)getWindow;
- (void)setSubDialog:(BOOL)subDialogVal;
- (DialogResult)getResult;

- (NSInteger)getCompression;
- (void)setCompression:(NSInteger)compression;
- (BOOL)getLumiChrom;
- (void)setLumiChrom:(BOOL)lumiChrom;
- (BOOL)getFloat;
- (void)setFloat:(BOOL)useFloat;
@end
