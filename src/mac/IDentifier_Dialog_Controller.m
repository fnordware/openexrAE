#import "IDentifier_Dialog_Controller.h"

@implementation IDentifier_Dialog_Controller

#ifndef __llvm__
@synthesize idMenu;
@synthesize window;
#endif

- (id)init {
	self = [super init];
	
	if(!([NSBundle loadNibNamed:@"IDentifier_Dialog" owner:self]))
		return nil;
	
	[self.window center];
	
	[self.idMenu setAutoenablesItems:FALSE];
	
	return self;
}

- (IBAction)clickedOK:(NSButton *)sender {
    [NSApp stopModal];
}

- (IBAction)clickedCancel:(NSButton *)sender {
    [NSApp abortModal];
}

- (NSPopUpButton *)getMenu {
	return self.idMenu;
}

- (NSWindow *)getWindow {
	return self.window;
}
@end
