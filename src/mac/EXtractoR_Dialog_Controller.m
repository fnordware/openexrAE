#import "EXtractoR_Dialog_Controller.h"

@implementation EXtractoR_Dialog_Controller

#ifndef __llvm__
@synthesize layerMenu;
@synthesize redMenu;
@synthesize greenMenu;
@synthesize blueMenu;
@synthesize alphaMenu;
@synthesize window;
@synthesize layers;
#endif

- (id)init:(NSArray *)chans
	layers:(NSDictionary *)lays
	red:(NSString *)r
	green:(NSString *)g
	blue:(NSString *)b
	alpha:(NSString *)a
{
	self = [super init];
	
	if(!([NSBundle loadNibNamed:@"EXtractoR_Dialog" owner:self]))
		return nil;

	[self.window center];

	NSPopUpButton *menus[] = { self.redMenu, self.greenMenu, self.blueMenu, self.alphaMenu };
	NSString *colors[] = { r, g, b, a };
	
	int i;
	
	for(i=0; i < 4; i++)
	{
		[menus[i] removeAllItems];
		[menus[i] addItemWithTitle:@"(copy)"];
		[menus[i] addItemsWithTitles:chans];
		[menus[i] selectItemWithTitle:colors[i]];
	}
	
	if([lays count] > 0)
	{
		NSEnumerator *layer_enum = [lays keyEnumerator];
		NSString *layer;
		
		while(layer = [layer_enum nextObject])
		{
			[self.layerMenu addItemWithTitle:layer];
		}
	}
	else
	{
		[self.layerMenu setEnabled:FALSE];
	}

	self.layers = lays;
	
	
	return self;
}

- (IBAction)clickOK:(NSButton *)sender {
    [NSApp stopModal];
}

- (IBAction)clickCancel:(NSButton *)sender {
    [NSApp abortModal];
}

- (IBAction)trackLayerMenu:(id)sender {
	NSString *layer_name = [[self.layerMenu selectedItem] title];
	
	NSArray *layer_array = [self.layers objectForKey:layer_name];
	
	NSPopUpButton *menus[] = { self.redMenu, self.greenMenu, self.blueMenu, self.alphaMenu };

	if(layer_array)
	{
		int i;
		
		if([layer_array count] == 1)
		{
			for(i=0; i < 3; i++)
			{
				[menus[i] selectItemWithTitle:[layer_array objectAtIndex:0]];
			}
			
			[menus[3] selectItemWithTitle:@"(copy)"];
		}
		else
		{
			for(i=0; i < 4; i++)
			{
				if(i < [layer_array count])
				{
					[menus[i] selectItemWithTitle:[layer_array objectAtIndex:i]];
				}
				else
					[menus[i] selectItemWithTitle:@"(copy)"];
			}
		}
	}
	else
	{
		[menus[0] selectItemWithTitle:@"R"];
		[menus[1] selectItemWithTitle:@"G"];
		[menus[2] selectItemWithTitle:@"B"];
		[menus[3] selectItemWithTitle:@"(copy)"];
	}
}

- (NSWindow *)getWindow {
	return self.window;
}

- (NSString *)getRed {
	return [[self.redMenu selectedItem] title];
}

- (NSString *)getGreen {
	return [[self.greenMenu selectedItem] title];
}

- (NSString *)getBlue {
	return [[self.blueMenu selectedItem] title];
}

- (NSString *)getAlpha {
	return [[self.alphaMenu selectedItem] title];
}

@end
