//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import "Cryptomatte_AE_Alt_Dialog_Controller.h"

#ifdef __llvm__
	#if __has_feature(objc_arc)
		#define USE_ARC 1
	#endif
#endif

@implementation KeyValue

#ifndef __llvm__
@synthesize key;
@synthesize value;
#endif

- (id)init:(NSString *)k withValue:(id)v
{
	self = [super init];
	
	if(self)
	{
		self.key = k;
		self.value = v;
	}
	
	return self;
}

+ (id)key:(NSString *)key withValue:(id)value
{
#ifdef USE_ARC
	return [[KeyValue alloc] init:key withValue:value];
#else
	return [[[KeyValue alloc] init:key withValue:value] autorelease];
#endif
}

@end


@interface Cryptomatte_AE_Alt_Dialog_Controller ()
- (NSArray *)treeObjectsFromNode:(KeyValue *)node;
- (NSArray *)selectedTreeObjects;
@end


@implementation Cryptomatte_AE_Alt_Dialog_Controller

#ifndef __llvm__
@synthesize list;
@synthesize tree;
@synthesize addButton;
@synthesize removeButton;
@synthesize selectionInfo;
@synthesize objectArray;
@synthesize objectTree;
#endif

- (id)init:(NSArray *)selection
	manifest:(NSArray *)manifest
{
	self = [super initWithWindowNibName:@"Cryptomatte_AE_Alt_Dialog"];
	
	if(self)
	{
		if(!([NSBundle loadNibNamed:@"Cryptomatte_AE_Alt_Dialog" owner:self]))
			return nil;
		
		[[self window] center];
		
		
		self.objectArray = manifest;
		
		[self.list setDataSource:self];
		
		
		// Our tree stucture uses these KeyValue objects as nodes, and the value can
		// be either an NSString (leaf) or an NSArray of more KeyValue objects (nodes).
		// It starts with a KeyValue at the root.
		// The ends (leafs) contain the full name of the original object, to be selected in
		// the list view.  Here's an example:
		
		/*KeyValue *tree_root = [KeyValue key:@"root" withValue:
									[NSArray arrayWithObjects:
										[KeyValue key:@"one" withValue:
												[NSArray arrayWithObjects:
													[KeyValue key:@"A" withValue:@"The actual value A"],
													[KeyValue key:@"B" withValue:@"The actual value B"],
													nil]],
										[KeyValue key:@"two" withValue:
												[NSArray arrayWithObjects:
													[KeyValue key:@"C" withValue:@"The actual value C"],
													[KeyValue key:@"D" withValue:@"The actual value D"],
													nil]],
										[KeyValue key:@"three" withValue:
												[NSArray arrayWithObjects:
													[KeyValue key:@"E" withValue:@"The actual value E"],
													[KeyValue key:@"F" withValue:@"The actual value F"],
													nil]],
										nil]];*/
		
		
		KeyValue *tree_root = [KeyValue key:@"root" withValue:@"temp"];
		
		NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:@":./"];
		
		for(NSString *item in manifest)
		{
			NSAssert([item isKindOfClass:[NSString class]], @"Expected NSString");
			
			NSArray *pathPartsRaw = [item componentsSeparatedByCharactersInSet:delimiters];
			
			NSMutableArray *pathParts = [NSMutableArray array]; // sans empty parts from double delimiters
			
			[pathPartsRaw enumerateObjectsUsingBlock:
							^(id obj, NSUInteger idx, BOOL *stop)
							{
								NSString *part = obj;
								
								NSAssert([part isKindOfClass:[NSString class]], @"Expected NSString");
								
								if([part length] > 0)
									[pathParts addObject:part];
							}];
			
			KeyValue *node = tree_root;
			
			for(NSUInteger i=0; i < [pathParts count]; i++)
			{
				NSString *pathPart = [pathParts objectAtIndex:i];
				
				NSAssert([pathPart isKindOfClass:[NSString class]], @"Expected NSString");
				
				id nodeVal = [node value];
				
				if([nodeVal isKindOfClass:[NSMutableArray class]])
				{
					NSMutableArray *nodeArray = nodeVal;
				
					const NSUInteger nextIdx = [nodeArray indexOfObjectPassingTest:
													^ BOOL (id obj, NSUInteger idx, BOOL *stop)
													{
														KeyValue *val = obj;
														
														NSAssert([val isKindOfClass:[KeyValue class]], @"Expected KeyValue");
														
														return [pathPart isEqualToString:[val key]];
													}];
													
					if(nextIdx == NSNotFound)
					{
						KeyValue *newItem = [KeyValue key:pathPart withValue:item];
						
						[nodeArray addObject:newItem];
						
						node = newItem;
					}
					else
					{
						node = [nodeArray objectAtIndex:nextIdx];
					}
				}
				else
				{
					NSMutableArray *newArray = [NSMutableArray arrayWithObjects:
													[KeyValue key:pathPart withValue:item],
													nil];
													
					[node setValue:newArray];
					
					node = [newArray objectAtIndex:0];
				}
			}
		}
						
		self.objectTree = tree_root;
		
		[self.tree setDataSource:self];
								
		
		
		NSMutableIndexSet *selected = [NSMutableIndexSet indexSet];
		
		for(NSString *item in selection)
		{
			NSAssert([item isKindOfClass:[NSString class]], @"Expected NSString");
			
			const NSUInteger idx = [self.objectArray indexOfObject:item];
			
			if(idx != NSNotFound)
				[selected addIndex:idx];
		}
		
		[self.list selectRowIndexes:selected byExtendingSelection:NO];
		
		
		
		[[NSNotificationCenter defaultCenter]
			addObserver:self
			selector:@selector(tableViewSelectionDidChange:)
			name:NSTableViewSelectionDidChangeNotification
			object:self.list];
		
		[[NSNotificationCenter defaultCenter]
			postNotificationName:NSTableViewSelectionDidChangeNotification
			object:self.list];
			
		[[NSNotificationCenter defaultCenter]
			addObserver:self
			selector:@selector(outlineViewSelectionDidChange:)
			name:NSOutlineViewSelectionDidChangeNotification
			object:self.tree];
		
		[[NSNotificationCenter defaultCenter]
			postNotificationName:NSOutlineViewSelectionDidChangeNotification
			object:self.tree];
	}
	
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter]
		removeObserver:self
		name:NSTableViewSelectionDidChangeNotification
		object:self.list];

	[self.list setDataSource:nil];
	
	self.objectArray = nil;
	
	[[NSNotificationCenter defaultCenter]
		removeObserver:self
		name:NSOutlineViewSelectionDidChangeNotification
		object:self.tree];
		
	[self.tree setDataSource:nil];
	
	self.objectTree = nil;

#ifndef USE_ARC
	[super dealloc];
#endif
}

- (IBAction)clickOK:(NSButton *)sender
{
    [NSApp stopModal];
}

- (IBAction)clickCancel:(NSButton *)sender
{
    [NSApp abortModal];
}

// NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
	return [self.objectArray count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
	return [self.objectArray objectAtIndex:row];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
	NSIndexSet *selected = [self.list selectedRowIndexes];
	
	[self.selectionInfo setStringValue:[NSString stringWithFormat:@"%lu / %lu selected", (unsigned long)[selected count], (unsigned long)[self.objectArray count]]];
}

// NSOutlineViewDataSource

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
	if(item == nil)
		item = self.objectTree;
	
	KeyValue *node = item;
	
	NSAssert([node isKindOfClass:[KeyValue class]], @"Expected KeyValue");
	
	NSArray *val = [node value];
	
	NSAssert([val isKindOfClass:[NSArray class]], @"Expected NSArray");
	NSAssert(index < [val count], @"Index out of range");
	
	return [val objectAtIndex:index];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
	if(item == nil)
		item = self.objectTree;
		
	KeyValue *node = item;
	
	NSAssert([node isKindOfClass:[KeyValue class]], @"Expected KeyValue");
	
	id val = [node value];
	
	return [val isKindOfClass:[NSArray class]];
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
	if(item == nil)
		item = self.objectTree;
	
	KeyValue *node = item;
	
	NSAssert([node isKindOfClass:[KeyValue class]], @"Expected KeyValue");
	
	NSArray *val = [node value];
	
	NSAssert([val isKindOfClass:[NSArray class]], @"Expected NSArray");
	
	return [val count];
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
	NSAssert(item != nil, @"Asked for value of root node");
		
	KeyValue *node = item;
	
	NSAssert([node isKindOfClass:[KeyValue class]], @"Expected KeyValue");
	
	return [node key];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
	const BOOL enabled = !![[self.tree selectedRowIndexes] count];
	
	[self.addButton setEnabled:enabled];
	[self.removeButton setEnabled:enabled];
}

- (NSArray *)treeObjectsFromNode:(KeyValue *)node
{
	NSMutableArray *objects = [NSMutableArray array];
	
	id val = [node value];
	
	if([val isKindOfClass:[NSString class]])
	{
		[objects addObject:val];
	}
	else if([val isKindOfClass:[NSArray class]])
	{
		NSArray *arr = val;
		
		[arr enumerateObjectsUsingBlock:
				^ (id obj, NSUInteger idx, BOOL *stop)
				{
					KeyValue *keyVal = obj;
					
					NSAssert([keyVal isKindOfClass:[KeyValue class]], @"Expected KeyValue");
					
					[objects addObjectsFromArray:[self treeObjectsFromNode:keyVal]];
				}];
	}
	else
		NSAssert(NO, @"Not an NSString or NSArray?");
	
	return objects;
}

- (NSArray *)selectedTreeObjects
{
	KeyValue *selectedNode = [self.tree itemAtRow:[self.tree selectedRow]];
	
	return [self treeObjectsFromNode:selectedNode];
}

- (IBAction)clickTreeAdd:(NSButton *)sender
{
	NSArray *objectsToAdd = [self selectedTreeObjects];
	
	NSMutableIndexSet *idxSet = [NSMutableIndexSet indexSet];
	
	for(NSString *objName in objectsToAdd)
	{
		NSAssert([objName isKindOfClass:[NSString class]], @"Expected NSString");
		
		const NSUInteger idx = [self.objectArray indexOfObjectPassingTest:
													^ BOOL (id obj, NSUInteger idx, BOOL *stop)
													{
														NSString *listObjName = obj;
														
														NSAssert([listObjName isKindOfClass:[NSString class]], @"Expected NSString");
														
														return [listObjName isEqualToString:objName];
													}];
		if(idx != NSNotFound)
		{
			[idxSet addIndex:idx];
		}
		else
			NSAssert(NO, @"Index not found?");
	}
	
	[self.list selectRowIndexes:idxSet byExtendingSelection:YES];
}

- (IBAction)clickTreeRemove:(NSButton *)sender
{
	NSArray *objectsToRemove = [self selectedTreeObjects];
	
	NSMutableIndexSet *idxSet = [[NSMutableIndexSet alloc] initWithIndexSet:[self.list selectedRowIndexes]];

#ifndef USE_ARC
	[idxSet autorelease];
#endif

	for(NSString *objName in objectsToRemove)
	{
		NSAssert([objName isKindOfClass:[NSString class]], @"Expected NSString");
		
		const NSUInteger idx = [self.objectArray indexOfObjectPassingTest:
													^ BOOL (id obj, NSUInteger idx, BOOL *stop)
													{
														NSString *listObjName = obj;
														
														NSAssert([listObjName isKindOfClass:[NSString class]], @"Expected NSString");
														
														return [listObjName isEqualToString:objName];
													}];
		if(idx != NSNotFound)
		{
			[idxSet removeIndex:idx];
		}
		else
			NSAssert(NO, @"Index not found?");
	}
	
	[self.list selectRowIndexes:idxSet byExtendingSelection:NO];
}

- (NSArray *)getSelection
{
	NSIndexSet *selected = [self.list selectedRowIndexes];
	
	NSMutableArray *selectionArray = [NSMutableArray arrayWithCapacity:[selected count]];
	
	[selected enumerateIndexesUsingBlock:
		^ (NSUInteger idx, BOOL *stop)
		{
			[selectionArray addObject:[self.objectArray objectAtIndex:idx]];
		}
	];
	
	return selectionArray;
}

@end

