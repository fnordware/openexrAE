#import "OpenEXR_OutUI_Controller.h"


@implementation OpenEXR_OutUI_Controller

#ifndef __llvm__
@synthesize theWindow;
@synthesize compressionPulldown;
@synthesize floatCheck;
@synthesize floatLabel;
@synthesize lumiChromCheck;
#endif

- (id)init {
	self = [super init];
	
	if(!([NSBundle loadNibNamed:@"OpenEXR_Dialog" owner:self]))
		return nil;
	
	[self.theWindow center];
	
	[self.compressionPulldown removeAllItems];
	[self.compressionPulldown addItemsWithTitles:
		[NSArray arrayWithObjects:@"None", @"RLE", @"Zip", @"Zip16", @"Piz", @"PXR24", @"B44", @"B44A", @"DWAA", @"DWAB", nil]];

	theResult = DIALOG_RESULT_CONTINUE;

	return self;
}

- (IBAction)trackLumiChrom:(id)sender {
	BOOL enabled =  ([self.lumiChromCheck state] == NSOffState);
	NSColor *label_color = (enabled ? [NSColor textColor] : [NSColor disabledControlTextColor]);
	
    [self.floatCheck setEnabled:enabled];
	[self.floatLabel setTextColor:label_color];
}

- (IBAction)clickOK:(id)sender {
	if(subDialog)
		theResult = DIALOG_RESULT_OK;
	else
		[NSApp stopModal];
}

- (IBAction)clickCancel:(id)sender {
	if(subDialog)
		theResult = DIALOG_RESULT_CANCEL;
	else
		[NSApp abortModal];
}

- (NSWindow *)getWindow {
	return self.theWindow;
}

- (void)setSubDialog:(BOOL)subDialogVal {
	subDialog = subDialogVal;
}

- (DialogResult)getResult {
	return theResult;
}

- (NSInteger)getCompression {
	return [self.compressionPulldown indexOfSelectedItem];
}

- (void)setCompression:(NSInteger)compression {
	[self.compressionPulldown selectItem:[self.compressionPulldown itemAtIndex:compression]];
}

- (BOOL)getLumiChrom {
	return ([self.lumiChromCheck state] == NSOnState);
}

- (void)setLumiChrom:(BOOL)lumiChrom {
	[self.lumiChromCheck setState:(lumiChrom ? NSOnState : NSOffState)];
	[self trackLumiChrom:nil]; // used to track the control when the dialog opens
}

- (BOOL)getFloat {
	return ([self.floatCheck state] == NSOnState);
}

- (void)setFloat:(BOOL)useFloat {
	[self.floatCheck setState:(useFloat ? NSOnState : NSOffState)];
}

@end
