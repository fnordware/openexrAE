//
//	OpenEXR file importer/exporter for After Effects (AEIO)
// 
//	by Brendan Bolles <brendan@fnordware.com>
//
//	see OpenEXR.cpp for more information
//

#ifndef OPENEXR_CHANNEL_CACHE_H
#define OPENEXR_CHANNEL_CACHE_H

#include "ImfHybridInputFile.h"

#include "OpenEXR_PlatformIO.h"

#include "fnord_SuiteHandler.h"

#include <IlmThreadMutex.h>

#include <list>
#include <time.h>


class CancelExc : public std::exception
{
  public:
	CancelExc(A_Err err) throw() : _err(err) {}
	virtual ~CancelExc() throw() {}
	
	A_Err err() const throw() { return _err; }
	virtual const char* what() const throw() { return "User cancelled"; }

  private:
	A_Err _err;
};


class OpenEXR_HeaderCache
{
  public:
	OpenEXR_HeaderCache(Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership, bool takeOwnership);
	virtual ~OpenEXR_HeaderCache();
	
	const int & parts() const { return _parts; }
	const Imf::Header & header() const { return _header; }
	const Imf::ChannelList & channels() const { return _channels; }
	const Imath::Box2i & dataWindow() const { return _dataWindow; }
	const Imath::Box2i & displayWindow() const { return _displayWindow; }
	
	const PathString & getPath() const { return _path; }
	DateTime getModTime() const { return _modtime; }
	
	void updateCacheTime();
	double cacheAge() const;
	bool cacheIsStale(int timeout) const;
	
	bool takeObjects(Imf::HybridInputFile * &in, IStreamPlatform * &stream);

  protected:
	IStreamPlatform * _stream;
	Imf::HybridInputFile * _infile;
	
  private:
	const int _parts;
	const Imf::Header _header;
	const Imf::ChannelList _channels;
	const Imath::Box2i _dataWindow;
	const Imath::Box2i _displayWindow;
	
	PathString _path;
	DateTime _modtime;

	time_t _last_access;
};


class OpenEXR_ChannelCache : public OpenEXR_HeaderCache
{
  public:
	OpenEXR_ChannelCache(const SPBasicSuite *pica_basicP, const AEIO_InterruptFuncs *inter,
							Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership, bool takeOwnership) :
								OpenEXR_HeaderCache(in, stream, tookOwnership, takeOwnership) {}
	virtual ~OpenEXR_ChannelCache() {}
	
	virtual A_Err fillFrameBuffer(const AEIO_InterruptFuncs *inter, const Imf::FrameBuffer &framebuffer, const Imath::Box2i &dw) = 0;
};


class OpenEXR_ChannelCacheMemory : public OpenEXR_ChannelCache
{
  public:
	OpenEXR_ChannelCacheMemory(const SPBasicSuite *pica_basicP, const AEIO_InterruptFuncs *inter,
							Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership);
	virtual ~OpenEXR_ChannelCacheMemory();
	
	virtual A_Err fillFrameBuffer(const AEIO_InterruptFuncs *inter, const Imf::FrameBuffer &framebuffer, const Imath::Box2i &dw);

  private:
	AEGP_SuiteHandler suites;
	
	int _width;
	int _height;
	
	typedef struct ChannelCache {
		Imf::PixelType	pix_type;
		AEIO_Handle		bufH;
		
		ChannelCache(Imf::PixelType t=Imf::HALF, AEIO_Handle b=NULL) : pix_type(t), bufH(b) {}
	} ChannelCache;
	
	typedef std::map<std::string, ChannelCache> ChannelMap;
	ChannelMap _cache;
	
	class AllocateMemTask;
};


class OpenEXR_ChannelCacheObjects : public OpenEXR_ChannelCache
{
  public:
	OpenEXR_ChannelCacheObjects(const SPBasicSuite *pica_basicP, const AEIO_InterruptFuncs *inter,
							Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership);
	virtual ~OpenEXR_ChannelCacheObjects() {}
	
	virtual A_Err fillFrameBuffer(const AEIO_InterruptFuncs *inter, const Imf::FrameBuffer &framebuffer, const Imath::Box2i &dw);
	
  private:
	AEGP_SuiteHandler suites;

#ifndef NDEBUG
	int _width;
	int _height;
#endif
};


class OpenEXR_CachePool
{
  public:
	OpenEXR_CachePool();
	~OpenEXR_CachePool();
	
	enum CacheStrategy
	{
		Strategy_Default = 0, // same as Strategy_MultiPart_Objects
		Strategy_MultiPart_Objects, // Multi-part as objects, single-part as memory
		Strategy_All_Objects,
		Strategy_All_Memory
	};
	
	void configurePool(int max_caches, CacheStrategy strategy=Strategy_Default, const SPBasicSuite *pica_basicP=NULL);
	
	OpenEXR_HeaderCache *findHeaderCache(const IStreamPlatform *stream);
	OpenEXR_HeaderCache *addHeaderCache(Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership);
	void returnHeaderCache(OpenEXR_HeaderCache *cache);

	OpenEXR_ChannelCache *findChannelCache(const IStreamPlatform *stream, const AEIO_InterruptFuncs *inter);
	OpenEXR_ChannelCache *addChannelCache(Imf::HybridInputFile *in, IStreamPlatform *stream, const AEIO_InterruptFuncs *inter, bool &tookOwnership);
	void returnChannelCache(OpenEXR_ChannelCache *cache);

	bool deleteStaleCaches(int timeout); // returns true if something was deleted
	
  private:
	int _max_caches;
	CacheStrategy _strategy;
	const SPBasicSuite *_pica_basicP;
	
	IlmThread::Mutex _mutex;
	std::list<OpenEXR_HeaderCache *> _headerPool;
	std::list<OpenEXR_ChannelCache *> _channelPool;
	int _headerCachesInUse;
	int _channelCachesInUse;
};


int ScanlineBlockSize(const Imf::HybridInputFile &in);
int TiledRowBlockSize(const Imf::HybridInputFile &in);


#endif // OPENEXR_CHANNEL_CACHE_H
