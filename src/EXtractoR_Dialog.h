//
//	EXtractoR
//		by Brendan Bolles <brendan@fnordware.com>
//
//	extract float channels
//
//	Part of the fnord OpenEXR tools
//		http://www.fnordware.com/OpenEXR/
//
//

#pragma once

#ifndef _EXTRACTOR_DIALOG_H_
#define _EXTRACTOR_DIALOG_H_

#include "EXtractoR.h"

#include <ImfChannelList.h>

#include <string>
#include <vector>
#include <map>

typedef std::vector<std::string> ChannelVec;
typedef std::map<std::string, ChannelVec> LayerMap;

#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION

bool EXtractoR_Dialog(
	const Imf::ChannelList	&channels,
	const LayerMap		&layers,
	std::string			&red,
	std::string			&green,
	std::string			&blue,
	std::string			&alpha,
	const char			*plugHndl,
	const void			*mwnd);

#else

typedef std::vector<std::string> MenuVec;

int PopUpMenu(const MenuVec &menu_items, int selected_index, const void *hwnd);

#endif // PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION


#endif // _EXTRACTOR_DIALOG_H_
