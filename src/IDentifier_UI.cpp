//
//	IDentifier
//		by Brendan Bolles <brendan@fnordware.com>
//
//	extract discrete UINT channels
//
//	Part of the fnord OpenEXR tools
//		http://www.fnordware.com/OpenEXR/
//
//
//	mmm...our custom UI
//
//

#include "IDentifier.h"


#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
#include "OpenEXR_UTF.h"
#else
#include "DrawbotBot.h"

#include <map>
#endif

using namespace std;

#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION

#define TEXT_COLOR      PF_App_Color_TEXT_DISABLED

#define kMENU_TEXT_INDENT_H  10
#define kMENU_TEXT_INDENT_V  2

#define kMENU_ARROW_WIDTH    14
#define kMENU_ARROW_HEIGHT   7

#define kMENU_ARROW_SPACE_H  8
#define kMENU_ARROW_SPACE_V  7

#define kMENU_SHADOW_OFFSET  3

static void DrawMenu(DrawbotBot &bot, const char *label, const char *item, unsigned int menu_width, bool layer_menu)
{
    DRAWBOT_PointF32 original = bot.Pos();
	
    float text_height = bot.FontSize();
	
    bot.Move(kMENU_LABEL_WIDTH, kMENU_TEXT_INDENT_V + text_height);
	
    bot.SetColor(TEXT_COLOR);
    bot.DrawString(label, kDRAWBOT_TextAlignment_Right);
	
    bot.Move(kMENU_LABEL_SPACE, -(kMENU_TEXT_INDENT_V + text_height));
	
    DRAWBOT_PointF32 menu_corner = bot.Pos();
	
    bot.Move(kMENU_SHADOW_OFFSET, kMENU_SHADOW_OFFSET);
    bot.SetColor(PF_App_Color_BLACK, 0.3f);
    bot.PaintRect(menu_width, kMENU_HEIGHT);
    bot.MoveTo(menu_corner);
	
    bot.SetColor(layer_menu ? PF_App_Color_SHADOW_DISABLED : PF_App_Color_SHADOW);
    bot.PaintRect(menu_width, kMENU_HEIGHT);
	
    bot.SetColor(PF_App_Color_HILITE);
    bot.DrawRect(menu_width, kMENU_HEIGHT);
	
    bot.Move(kMENU_TEXT_INDENT_H, kMENU_TEXT_INDENT_V + text_height);
	
    bot.SetColor(TEXT_COLOR);
    bot.DrawString(item,
                    kDRAWBOT_TextAlignment_Left,
                    kDRAWBOT_TextTruncation_EndEllipsis,
                    menu_width - kMENU_TEXT_INDENT_H - kMENU_TEXT_INDENT_H -
                        kMENU_ARROW_WIDTH - kMENU_ARROW_SPACE_H - kMENU_ARROW_SPACE_H);
	
    bot.MoveTo(menu_corner.x + menu_width - kMENU_ARROW_SPACE_H - kMENU_ARROW_WIDTH,
                menu_corner.y + kMENU_ARROW_SPACE_V);
	
    bot.SetColor(PF_App_Color_LIGHT_TINGE);
    bot.PaintTriangle(kMENU_ARROW_WIDTH, kMENU_ARROW_HEIGHT);
	
    bot.MoveTo(original);
}


static PF_Err
DoClick(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	event_extra->evt_out_flags = PF_EO_NONE;
	
	if(event_extra->effect_win.area == PF_EA_CONTROL)
	{
        PF_Point local_point;
		
        local_point.h = event_extra->u.do_click.screen_point.h - event_extra->effect_win.current_frame.left;
        local_point.v = event_extra->u.do_click.screen_point.v - event_extra->effect_win.current_frame.top;
		
		const PF_Rect &control_rect = event_extra->effect_win.current_frame;
		
		const unsigned int menu_width = MAX(kMENU_WIDTH, control_rect.right - RIGHT_PLACE(control_rect.left) - kMENU_LABEL_WIDTH - kMENU_LABEL_SPACE - kMENU_SHADOW_OFFSET);

        if(local_point.h > RIGHT_PLACE(control_rect.left) && local_point.h < (RIGHT_PLACE(control_rect.left) + menu_width))
        {
			if(local_point.v > kUI_CONTROL_DOWN && local_point.v < (kUI_CONTROL_DOWN + kMENU_HEIGHT))
			{
				if((local_point.v - kUI_CONTROL_DOWN) < kMENU_HEIGHT)
				{
					AEGP_SuiteHandler suites(in_data->pica_basicP);
					PF_ChannelSuite *cs = suites.PFChannelSuite();

					A_long chan_count = 0;
					cs->PF_GetLayerChannelCount(in_data->effect_ref, 0, &chan_count);

					if(chan_count == 0 || err)
					{
						PF_SPRINTF(out_data->return_msg, "No auxiliary channels available.");
					}
					else
					{
						ArbitraryData *arb_data = (ArbitraryData *)PF_LOCK_HANDLE(params[IDENTIFIER_DATA]->u.arb_d.value);
						ChannelStatus *channel_status = (ChannelStatus *)PF_LOCK_HANDLE(in_data->sequence_data);

						// reconcile incorrect index
						if(channel_status->status == STATUS_INDEX_CHANGE)
							arb_data->index = channel_status->index;

					#ifdef MAC_ENV
						const void *hwnd = NULL;
					#else
						HWND *hwnd = NULL;
						PF_GET_PLATFORM_DATA(PF_PlatData_MAIN_WND, &hwnd);
					#endif
						
						MenuVec menuVec;
						int selected_idx = -1;
						
						map<string, int> index_map;
						
						for(int i=0; i<chan_count; i++)
						{
							PF_Boolean	found;

							PF_ChannelRef chan_ref;
							PF_ChannelDesc chan_desc;
							
							// get the channel
							cs->PF_GetLayerChannelIndexedRefAndDesc(in_data->effect_ref,
																	0,
																	i,
																	&found,
																	&chan_ref,
																	&chan_desc);
							
							// found isn't really what it used to be
							found = (found && chan_desc.channel_type && chan_desc.data_type);
							
							if(found && chan_desc.dimension == 1 &&
								(	chan_desc.data_type == PF_DataType_CHAR ||
									chan_desc.data_type == PF_DataType_U_BYTE ||
									chan_desc.data_type == PF_DataType_U_SHORT ||
									chan_desc.data_type == PF_DataType_LONG  ))
							{
								menuVec.push_back(chan_desc.name);
								
								index_map[chan_desc.name] = i;
								
								if(string(chan_desc.name) == arb_data->name)
									selected_idx = menuVec.size() - 1;
							}
						}


						if(menuVec.size() == 0)
						{
							PF_SPRINTF(out_data->return_msg, "No ID channels available.");
						}
						else
						{
							const int popUpChoice = PopUpMenu(menuVec, selected_idx, hwnd);
							
							if(popUpChoice >= 0)
							{
								arb_data->index = index_map[menuVec[popUpChoice]];
								strncpy(arb_data->name, menuVec[popUpChoice].c_str(), MAX_CHANNEL_NAME_LEN);
								
								params[IDENTIFIER_DATA]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
							}
						}

						
						event_extra->evt_out_flags = PF_EO_HANDLED_EVENT;
						
						PF_UNLOCK_HANDLE(params[IDENTIFIER_DATA]->u.arb_d.value);
						PF_UNLOCK_HANDLE(in_data->sequence_data);
					}
				}
			}
        }
	}
	
	return err;
}

#endif // PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION

static PF_Err
DrawEvent(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err			err		=	PF_Err_NONE;
	
	AEGP_SuiteHandler suites(in_data->pica_basicP);
	
	event_extra->evt_out_flags = 0;

	if (!(event_extra->evt_in_flags & PF_EI_DONT_DRAW) && params[IDENTIFIER_DATA]->u.arb_d.value != NULL)
	{
		if(PF_EA_CONTROL == event_extra->effect_win.area)
		{
			ArbitraryData *arb_data = (ArbitraryData *)PF_LOCK_HANDLE(params[IDENTIFIER_DATA]->u.arb_d.value);
			ChannelStatus *channel_status = (ChannelStatus *)PF_LOCK_HANDLE(in_data->sequence_data);
		
		#define MAX_MESSAGE_LEN 127
			A_char	message[MAX_MESSAGE_LEN];
					
			// start with the name
			strcpy(message, arb_data->name);
			
			// process status returned from Render()
			if(channel_status->status == STATUS_NOT_FOUND)
			{
				strcat(message, " (not found)");
			}
			else if(channel_status->status == STATUS_NO_CHANNELS)
			{
				strcat(message, " (none available)");
			}
			else if(channel_status->status == STATUS_ERROR)
			{
				strcat(message, " (error)");
			}
			
			const PF_Rect &control_rect = event_extra->effect_win.current_frame;

#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION

		// old-school Carbon/Win32 drawing code

		#ifdef WIN_ENV
			void*			dp = (*(event_extra->contextH))->cgrafptr;
			HDC				ecw_hdc	=	0;
			PF_GET_CGRAF_DATA(dp, PF_CGrafData_HDC, (void **)&ecw_hdc);
			PF_Point		pen;
		#endif
	
			PF_App_Color	pf_bg_color;
			suites.AppSuite()->PF_AppGetBgColor(&pf_bg_color);
			
		#ifdef MAC_ENV
			RGBColor titleColor, itemColor;
			
			RGBColor		bg_color;
			bg_color.red	=	pf_bg_color.red;
			bg_color.green	=	pf_bg_color.green;
			bg_color.blue	=	pf_bg_color.blue;
		
			RGBBackColor(&bg_color);
		
			EraseRect(&control_rect);
			
			GetForeColor(&itemColor);
			
			titleColor.red   = TITLE_COMP(itemColor.red);
			titleColor.green = TITLE_COMP(itemColor.green);
			titleColor.blue  = TITLE_COMP(itemColor.blue);
				
			RGBForeColor(&titleColor);
		#endif
		
		#ifdef MAC_ENV
		#define DRAW_STRING(STRING)	DrawText(STRING, 0, strlen(STRING));
		#define MOVE_TO(H, V)		MoveTo( (H), (V) );
		#else
		#define DRAW_STRING(STRING) \
			do{ \
				WCHAR u_string[100]; \
				UTF8toUTF16(STRING, (utf16_char *)u_string, 99); \
				TextOut(ecw_hdc, pen.h, pen.v, u_string, strlen(STRING)); \
			}while(0);
		#define MOVE_TO(H, V)		MoveToEx(ecw_hdc, (H), (V), NULL); pen.h = (H); pen.v = (V);
		#endif
		
			// Property Heading
			MOVE_TO(RIGHT_STATUS(control_rect.left), DOWN_PLACE(control_rect.top) );
			DRAW_STRING("Channel");
			
			//Property Items
			PF_Point prop_origin;
			prop_origin.v = control_rect.top;
			
	#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
		#ifdef MAC_ENV
			RGBForeColor(&itemColor);
			prop_origin.h = control_rect.left + TextWidth("Alpha", 0, strlen("Alpha"));
		#else
			SIZE text_dim = {15, 8};
			GetTextExtentPoint(ecw_hdc, TEXT("Alpha"), strlen("Alpha"), &text_dim);
			prop_origin.h = control_rect.left + text_dim.cx;
		#endif
	#else
			current_brush = &item_brushP;
			prop_origin.h = control_rect.left + 50;
	#endif
			
			MOVE_TO(RIGHT_PROP(prop_origin.h), DOWN_PLACE(prop_origin.v) );
			DRAW_STRING(message);

#else // PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION

			// new-school Drawbot drawing code

			DrawbotBot bot(in_data->pica_basicP, event_extra->contextH, in_data->appl_id);
			
			const unsigned int menu_width = MAX(kMENU_WIDTH, control_rect.right - RIGHT_PLACE(control_rect.left) - kMENU_LABEL_WIDTH - kMENU_LABEL_SPACE - kMENU_SHADOW_OFFSET);
			
			bot.MoveTo(control_rect.left + kUI_CONTROL_RIGHT, control_rect.top + kUI_CONTROL_DOWN);
			DrawMenu(bot, "Channel:", message, menu_width, false);
			
#endif // PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION

			event_extra->evt_out_flags = PF_EO_HANDLED_EVENT;

			PF_UNLOCK_HANDLE(params[IDENTIFIER_DATA]->u.arb_d.value);
			PF_UNLOCK_HANDLE(in_data->sequence_data);
		}
	}

	return err;
}



PF_Err
HandleEvent ( 
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*extra )
{
	PF_Err		err		= PF_Err_NONE;
	
	if (!err) 
	{
		switch (extra->e_type) 
		{
			case PF_Event_DRAW:
				err = DrawEvent(in_data, out_data, params, output, extra);
				break;
			
			case PF_Event_DO_CLICK:
			#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
				err = DoDialog(in_data, out_data, params, output);
				extra->evt_out_flags = PF_EO_HANDLED_EVENT;
			#else
				err = DoClick(in_data, out_data, params, output, extra);
			#endif
				break;
				
			case PF_Event_ADJUST_CURSOR:
			#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
			#if defined(MAC_ENV)
				#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
				SetMickeyCursor(); // the cute mickey mouse hand
				#else
				SetThemeCursor(kThemePointingHandCursor);
				#endif
				extra->u.adjust_cursor.set_cursor = PF_Cursor_CUSTOM;
			#else
				extra->u.adjust_cursor.set_cursor = PF_Cursor_FINGER_POINTER;
			#endif
				extra->evt_out_flags = PF_EO_HANDLED_EVENT;
			#endif
				break;
		}
	}
	
	return err;
}
