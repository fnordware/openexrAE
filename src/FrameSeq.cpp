//
//	OpenEXR file importer/exporter for After Effects (AEIO)
// 
//	by Brendan Bolles <brendan@fnordware.com>
//
//	see OpenEXR.cpp for more information
//

#include "FrameSeq.h"

#include "OpenEXR.h"

#include "OpenEXR_UTF.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <assert.h>

#ifdef WIN_ENV
#include <Windows.h>
#endif


class ErrThrower : public std::exception
{
  public:
	ErrThrower(A_Err err = A_Err_NONE) throw() : _err(err) {}
	ErrThrower(const ErrThrower &other) throw() : _err(other._err) {}
	virtual ~ErrThrower() throw() {}

	ErrThrower & operator = (A_Err err)
	{
		_err = err;
		
		if(_err != A_Err_NONE)
			throw *this;
		
		return *this;
	}

	A_Err err() const { return _err; }
	
	virtual const char* what() const throw() { return "AE Error"; }
	
  private:
	A_Err _err;
};


extern AEGP_PluginID			S_mem_id;

static A_short					gDriverImplVersionMajor = 0;
static A_short					gDriverImplVersionMinor = 0;


// edit these typedefs for each custom file importer
typedef	OpenEXR_inData		format_inData;
typedef	OpenEXR_outData		format_outData;


static A_Err
EasyCopy(AEIO_BasicData	*basic_dataP, PF_EffectWorld *src, PF_EffectWorld *dest, PF_Boolean hq)
{
	A_Err err =	A_Err_NONE;
	
	AEGP_SuiteHandler suites(basic_dataP->pica_basicP);

	if(hq)
		err = suites.PFWorldTransformSuite()->copy_hq(NULL, src, dest, NULL, NULL);
	else
		err = suites.PFWorldTransformSuite()->copy(NULL, src, dest, NULL, NULL);
	
	return err;
}

static A_Err
SmartCopyWorld(
	AEIO_BasicData		*basic_dataP,
	PF_EffectWorld 		*source_World,
	PF_EffectWorld 		*dest_World,
	PF_Boolean 			source_ext,
	PF_Boolean	 		dest_ext, // _ext means 16bpc is true 16-bit (the EXTernal meaning of 16-bit)
	PF_Boolean			source_writeable)
{
	// this is the world copy function AE should have provided
	// with additional attention to external (true) 16-bit worlds
	A_Err ae_err =	A_Err_NONE;
	
	AEGP_SuiteHandler suites(basic_dataP->pica_basicP);
	

	PF_EffectWorld temp_World_data;
	PF_EffectWorld *temp_World = NULL;
		
	try{
	
	ErrThrower err;
	
	PF_PixelFormat	source_format, dest_format;

	err = suites.PFWorldSuite()->PF_GetPixelFormat(source_World, &source_format);
	err = suites.PFWorldSuite()->PF_GetPixelFormat(dest_World, &dest_format);
	
	PF_Boolean hq = FALSE;			
	
	// can we just copy?
	if( (source_format == dest_format) && (source_ext == dest_ext) ) // make sure you are always setting ext's
	{
		err = EasyCopy(basic_dataP, source_World, dest_World, hq);
	}
	else
	{
		// copy to a buffer of the same size, different bit depth
		PF_EffectWorld *active_World = NULL;
		
		// if out world is the same size, we'll copy directly, otherwise need temp
		if( (source_World->height == dest_World->width) &&
			(source_World->width  == dest_World->width) )
		{
			active_World = dest_World;
		}
		else
		{
			temp_World = &temp_World_data;

			err = suites.PFWorldSuite()->PF_NewWorld(NULL, source_World->width, source_World->height,
													FALSE, dest_format, temp_World);

			active_World = temp_World;
		}

		char *in_row = (char *)source_World->data,
			*out_row = (char *)active_World->data;

		const int height = source_World->height;
		const int width4 = source_World->width * 4;

		for(int y=0; y < height; y++)
		{
			if(source_format == PF_PixelFormat_ARGB128)
			{
				PF_FpShort *in = (PF_FpShort *)in_row;

				if(dest_format == PF_PixelFormat_ARGB64)
				{
					A_u_short *out = (A_u_short *)out_row;

					if(dest_ext)
						for(int x=0; x < width4; x++)
						{	*out = FLOAT_TO_SIXTEEN(*in); out++; in++;	}
					else
						for(int x=0; x < width4; x++)
						{	*out = FLOAT_TO_AE(*in); out++; in++;	}
				}
				else if(dest_format == PF_PixelFormat_ARGB32)
				{
					A_u_char *out = (A_u_char *)out_row;

					for(int x=0; x < width4; x++)
					{	*out = FLOAT_TO_AE8(*in); out++; in++;	}
				}
			}
			else if(source_format == PF_PixelFormat_ARGB64)
			{
				A_u_short *in = (A_u_short *)in_row;

				if(source_ext)
				{
					if(dest_format == PF_PixelFormat_ARGB128)
					{
						PF_FpShort *out = (PF_FpShort *)out_row;

						for(int x=0; x < width4; x++)
						{	*out = SIXTEEN_TO_FLOAT(*in); out++; in++;	}
					}
					else if(dest_format == PF_PixelFormat_ARGB32)
					{
						A_u_char *out = (A_u_char *)out_row;

						for(int x=0; x < width4; x++)
						{	*out = *in >> 8; out++; in++;	}
					}
				}
				else
				{
					if(dest_format == PF_PixelFormat_ARGB128)
					{
						PF_FpShort *out = (PF_FpShort *)out_row;

						for(int x=0; x < width4; x++)
						{	*out = AE_TO_FLOAT(*in); out++; in++;	}
					}
					else if(dest_format == PF_PixelFormat_ARGB32)
					{
						A_u_char *out = (A_u_char *)out_row;

						for(int x=0; x < width4; x++)
						{	*out = CONVERT16TO8(*in); out++; in++;	}
					}
				}

			}
			else if(source_format == PF_PixelFormat_ARGB32)
			{
				A_u_char *in = (A_u_char *)in_row;

				if(dest_format == PF_PixelFormat_ARGB128)
				{
					PF_FpShort *out = (PF_FpShort *)out_row;

					for(int x=0; x < width4; x++)
					{	*out = AE8_TO_FLOAT(*in); out++; in++;	}
				}
				else if(dest_format == PF_PixelFormat_ARGB64)
				{
					A_u_char *out = (A_u_char *)out_row;

					if(dest_ext)
						for(int x=0; x < width4; x++)
						{	*out = ( ((long)*in * 0xFFFF) + PF_HALF_CHAN8 ) / PF_MAX_CHAN8; out++; in++;	} // uhh, I think this works
					else
						for(int x=0; x < width4; x++)
						{	*out = CONVERT8TO16(*in); out++; in++;	}
				}
			}

			in_row += source_World->rowbytes;
			out_row += active_World->rowbytes;
		}

		// copy from temp world if necessary
		if(active_World != dest_World)
		{
			err = EasyCopy(basic_dataP, temp_World, dest_World, hq);
		}
	}
	
	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }

	if(temp_World)
		suites.PFWorldSuite()->PF_DisposeWorld(NULL, temp_World);

	return ae_err;
}

#ifdef AE_HFS_PATHS
// convert from HFS paths (fnord:Users:mrb:) to Unix paths (/Users/mrb/)
static int ConvertPath(const char * inPath, char * outPath, int outPathMaxLen)
{
	CFStringRef inStr = CFStringCreateWithCString(kCFAllocatorDefault, inPath ,kCFStringEncodingMacRoman);
	if (inStr == NULL)
		return -1;
	CFURLRef url = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, inStr, kCFURLHFSPathStyle,0);
	if (url == NULL)
		return -1;
	CFStringRef outStr = CFURLCopyFileSystemPath(url, kCFURLPOSIXPathStyle);
	if (!CFStringGetCString(outStr, outPath, outPathMaxLen, kCFStringEncodingMacRoman))
		return -1;
	CFRelease(outStr);
	CFRelease(url);
	CFRelease(inStr);
	return 0;
}
#endif // __MACH__

// platform-specific calls to get the file size
static void GetFileSize(const A_PathType *path, A_long *size)
{
#ifdef MAC_ENV
	OSStatus status = noErr;
	FSRef fsref;
	
	// expecting a POSIX path here
#ifdef AE_UNICODE_PATHS	
	int len = -1;
	while(path[++len] != 0);
	
	CFStringRef inStr = CFStringCreateWithCharacters(kCFAllocatorDefault, path, len);
	if (inStr == NULL)
		return;
		
	CFURLRef url = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, inStr, kCFURLPOSIXPathStyle, 0);
	CFRelease(inStr);
	if (url == NULL)
		return;
	
	Boolean success = CFURLGetFSRef(url, &fsref);
	CFRelease(url);
	if(!success)
		return;
#else	
	status = FSPathMakeRef((const UInt8 *)path, &fsref, false);	
#endif

	if(status == noErr)
	{
		OSErr result = noErr;

	#if MAC_OS_X_VERSION_MAX_ALLOWED < 1050
		typedef SInt16 FSIORefNum;
	#endif
		
		HFSUniStr255 dataForkName;
		FSIORefNum refNum;
		SInt64 fork_size;
	
		result = FSGetDataForkName(&dataForkName);

		result = FSOpenFork(&fsref, dataForkName.length, dataForkName.unicode, fsRdPerm, &refNum);
		
		result = FSGetForkSize(refNum, &fork_size);
		
		result = FSCloseFork(refNum);
		
		*size = MIN(INT_MAX, fork_size);
	}
#else
#ifdef AE_UNICODE_PATHS
	HANDLE hFile = CreateFileW((LPCWSTR)path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
#else
	HANDLE hFile = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
#endif

	if(hFile)
	{
		*size = GetFileSize(hFile, NULL);

		CloseHandle(hFile);
	}
#endif
}


static void
SetRenderInfo(
	AEIO_BasicData	*basic_dataP,
	AEIO_OutSpecH	outH,
	Render_Info		*render_info)
{
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);
	
	render_info->proj_name[0] = '\0';
	render_info->comp_name[0] = '\0';
	render_info->comment_str[0] = '\0';
	render_info->computer_name[0] = '\0';
	render_info->user_name[0] = '\0';

#ifdef GET_PROJECT_AND_COMP_INFO
	AEGP_RQItemRefH rq_itemH = NULL;
	AEGP_OutputModuleRefH outmodH = NULL;
	
	// apparently 0x00 is a valid value for rq_itemH and outmodH
	A_Err err = suites.IOOutSuite()->AEGP_GetOutSpecOutputModule(outH, &rq_itemH, &outmodH);
	
	if(!err)
	{
		// get the comment
		suites.RQItemSuite()->AEGP_GetComment(rq_itemH, render_info->comment_str);
		
		// comp name
		AEGP_CompH compH = NULL;
		suites.RQItemSuite()->AEGP_GetCompFromRQItem(rq_itemH, &compH);
		
		if(compH) // should never fail, but to be safe
		{
			AEGP_ItemH itemH = NULL;
			
			suites.CompSuite()->AEGP_GetItemFromComp(compH, &itemH);
			
			if(itemH)
			{
			#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
				AEGP_MemHandle nameH = NULL;
				
				suites.ItemSuite()->AEGP_GetItemName(S_mem_id, itemH, &nameH);
				
				if(nameH)
				{
					AEGP_MemSize size = 0;
					suites.MemorySuite()->AEGP_GetMemHandleSize(nameH, &size);
					
					A_UTF16Char *nameP = NULL;
					suites.MemorySuite()->AEGP_LockMemHandle(nameH, (void **)&nameP);
					
					std::string comp_name = UTF16toUTF8(nameP);
					
					strncpy(render_info->comp_name, comp_name.c_str(), COMP_NAME_SIZE-1);
					
					suites.MemorySuite()->AEGP_FreeMemHandle(nameH);
				}
			#else
				suites.ItemSuite()->AEGP_GetItemName(itemH, render_info->comp_name);
			#endif
			}
			
			// frame duration
			if(!err)
			{
				AEGP_MemHandle formatH = NULL, infoH = NULL;
				A_Boolean is_sequence = TRUE, multi_frame = TRUE;
			
				suites.OutputModuleSuite()->AEGP_GetExtraOutputModuleInfo(rq_itemH, outmodH,
														&formatH, &infoH, &is_sequence, &multi_frame);
				
				if(multi_frame)
				{
					A_Time frame_duration;
					suites.CompSuite()->AEGP_GetCompFrameDuration(compH, &frame_duration);
					
					// frame rate = 1 / frame duration
					//render_info->framerate.value = frame_duration.scale;
					//render_info->framerate.scale = frame_duration.value;
				}
				
				if(formatH) // just says "OpenEXR"
					suites.MemorySuite()->AEGP_FreeMemHandle(formatH);

				if(infoH) // says something like "B44A compression\nLuminance/Chroma"
					suites.MemorySuite()->AEGP_FreeMemHandle(infoH);
			}
		}
	}	
		
	// project name
	A_long projects = 0;
	suites.ProjSuite()->AEGP_GetNumProjects(&projects);
	
	if(projects == 1)
	{
		AEGP_ProjectH projH = NULL;
		
		suites.ProjSuite()->AEGP_GetProjectByIndex(0, &projH);
		
		if(projH)
			suites.ProjSuite()->AEGP_GetProjectName(projH, render_info->proj_name);
	}
#endif // GET_PROJECT_AND_COMP_INFO

#ifdef MAC_ENV
	// user and computer name
	CFStringRef user_name = CSCopyUserName(FALSE);  // set this to TRUE for shorter unix-style name
	CFStringRef comp_name = CSCopyMachineName();
	
	if(user_name)
	{
		CFStringGetCString(user_name, render_info->user_name, COMPUTER_NAME_SIZE-1, kCFStringEncodingMacRoman);
		CFRelease(user_name);
	}
	
	if(comp_name)
	{
		CFStringGetCString(comp_name, render_info->computer_name, COMPUTER_NAME_SIZE-1, kCFStringEncodingMacRoman);
		CFRelease(comp_name);
	}
#else // WIN_ENV
	DWORD buf_size = COMPUTER_NAME_SIZE-1;
	GetComputerName(render_info->computer_name, &buf_size);
	buf_size = COMPUTER_NAME_SIZE-1;
	GetUserName(render_info->user_name, &buf_size);
#endif
}


#ifdef AE170_TIMECODE_SUITES
static A_Boolean
ParseTimeCodeString(const A_char *str, Time_Code *timeCode)
{
	int parts[4] = {0, 0, 0, 0};
	int current_part = 0;
	int current_part_start = -1;
	A_Boolean drop_frame = FALSE;
	A_Boolean negative = FALSE;

	int i = 0;
	
	while(i < 16)
	{
		if(str[i] == '-')
		{
			if(i == 0)
				negative = TRUE;
			else
				return FALSE;
		}
		else if(str[i] >= '0' && str[i] <= '9')
		{
			if(current_part_start < 0)
				current_part_start = i;
		}
		else if(str[i] == ':' || str[i] == ';' || str[i] == '\0')
		{
			if(current_part_start >= 0 && current_part_start < i && current_part < 4)
			{
				A_char temp[16];
				
				strncpy(temp, &str[current_part_start], i - current_part_start);
				
				parts[current_part++] = atoi(temp);
				
				current_part_start = -1;
			}
			else
				return FALSE;
			
			if(str[i] == ';')
				drop_frame = TRUE;
			else if(str[i] == '\0')
				break;
		}
		else
			return FALSE;
		
		i++;
	}
	
	const int total_parts = current_part;
	
	if(total_parts < 1)
		return FALSE;
	
	assert(total_parts == 4);
	
	timeCode->frame = parts[total_parts - 1];
	
	timeCode->seconds = (total_parts >= 2 ? parts[total_parts - 2] : 0);

	timeCode->minutes = (total_parts >= 3 ? parts[total_parts - 3] : 0);

	timeCode->hours = (total_parts == 4 ? parts[total_parts - 4] : 0);
	
	assert(timeCode->frame >= 0);
	assert(timeCode->seconds >= 0 && timeCode->seconds < 60);
	assert(timeCode->minutes >= 0 && timeCode->minutes < 60);
	assert(timeCode->hours >= 0);
	
	timeCode->drop_frame = drop_frame;
	timeCode->negative = negative;

	return TRUE;
}


static void
TimeCodeString(const Time_Code *timeCode, A_char *str)
{
	const char *sep = (timeCode->drop_frame ? ";" : ":");
	const char *neg = (timeCode->negative ? "-" : "");

	snprintf(str, 16, "%s%02d%s%02d%s%02d%s%02d", neg, timeCode->hours, sep, timeCode->minutes, sep, timeCode->seconds, sep, timeCode->frame);
}


static void
CalculateStartTime(const Time_Code *timeCode, A_Ratio *frame_rate, A_Time *start_time)
{
	const int time_base = ((double)frame_rate->num / (double)frame_rate->den) + 0.5;
	
	int dropped_frames = 0;
	
	if(timeCode->drop_frame)
	{
		const int minutes = (60 * timeCode->hours) + timeCode->minutes;
		
		dropped_frames = (minutes - (minutes / 10)) * (time_base == 60 ? 4 : 2);
	}
	
	const A_long frame = ((60 * 60 * time_base * timeCode->hours) +
							(60 * time_base * timeCode->minutes) +
							(time_base * timeCode->seconds) +
							timeCode->frame -
							dropped_frames) *
							(timeCode->negative ? -1 : 1);
	
	start_time->value = (frame * frame_rate->den);
	start_time->scale = frame_rate->num;
}


static A_long
CalculateFrameNumber(A_Time start_time, A_Time frame_time, A_Ratio frame_rate)
{
	// use AE frame rate ratios
	if(frame_rate.den == 1001)
	{
		if(frame_rate.num == 24000)
		{
			frame_rate.num = 2997;
			frame_rate.den = 125;
		}
		else if(frame_rate.num == 30000)
		{
			frame_rate.num = 2997;
			frame_rate.den = 100;
		}
		else if(frame_rate.num == 60000)
		{
			frame_rate.num = 2997;
			frame_rate.den = 50;
		}
	}
	
	A_long frame = 0;
	
	if((start_time.scale == frame_rate.num) && (start_time.value % (A_long)frame_rate.den == 0))
	{
		frame += (start_time.value / (A_long)frame_rate.den);
	}
	else
	{
		// not rounding if start time is AE's max value (24 hours - 1 minute) to match what AE is doing
		const double rounder = ((start_time.value == 86340 && start_time.scale == 1) ? 0.0 : (start_time.value < 0 ? -0.5 : 0.5));
		
		frame += (A_long)((((double)start_time.value / (double)start_time.scale) * ((double)frame_rate.num / (double)frame_rate.den)) + rounder);
	}
	
	if((frame_time.scale == frame_rate.num) && (frame_time.value % (A_long)frame_rate.den == 0))
	{
		frame += (frame_time.value / (A_long)frame_rate.den);
	}
	else
	{
		const double rounder = (frame_time.value < 0 ? -0.5 : 0.5);

		frame += (A_long)((((double)frame_time.value / (double)frame_time.scale) * ((double)frame_rate.num / (double)frame_rate.den)) + rounder);
	}
	
	return frame;
}


static void
CalculateTimeCode(A_long frame_num, A_u_long frames_per_second, A_Boolean drop_frame, Time_Code *time_code)
{
	if(frame_num < 0)
	{
		time_code->negative = TRUE;
		frame_num = -frame_num;
	}
	else
		time_code->negative = FALSE;
	
	int h = 0,
		m = 0,
		s = 0,
		f = 0;
	
	const int frames_per_min = (frames_per_second * 60) - (drop_frame ? (frames_per_second == 60 ? 4 : 2) : 0);
	const int frames_per_ten_mins = (frames_per_second * 60 * 10) - (drop_frame ? 9 * (frames_per_second == 60 ? 4 : 2) : 0);
	const int frames_per_hour = 6 * frames_per_ten_mins;
	
	h = frame_num / frames_per_hour;
	frame_num -= h * frames_per_hour;
	
	const int tenMin = frame_num / frames_per_ten_mins;
	m += 10 * tenMin;
	frame_num -= tenMin * frames_per_ten_mins;
	
	const int oneMin = frame_num / frames_per_min;
	m += oneMin;
	frame_num -= oneMin * frames_per_min;
	
	// if we don't take into account the non-dropped frames in minute 0, how many frames are we skipping with a simple frames_per_min?
	const int skippedFrames = ((drop_frame && oneMin > 0) ? (frames_per_second == 60 ? 4 : 2) : 0);
	
	if(skippedFrames > frame_num)
	{
		m--;
		frame_num += frames_per_min;
	}
	
	s = frame_num / frames_per_second;
	frame_num -= s * frames_per_second;
	
	f = frame_num;
	

	time_code->hours = h;
	time_code->minutes = m;
	time_code->seconds = s;
	time_code->frame = f;
	time_code->drop_frame = drop_frame;
}
#endif // AE170_TIMECODE_SUITES

#pragma mark-

A_Err
FrameSeq_Init(struct SPBasicSuite *pica_basicP)
{
	AEGP_SuiteHandler suites(pica_basicP);

	suites.UtilitySuite()->AEGP_GetDriverImplementationVersion(&gDriverImplVersionMajor, &gDriverImplVersionMinor);
	
	return OpenEXR_Init(pica_basicP);
}


A_Err
FrameSeq_DeathHook(const SPBasicSuite *pica_basicP)
{
	return OpenEXR_DeathHook(pica_basicP);
}


A_Err
FrameSeq_IdleHook(AEIO_BasicData *basic_dataP, AEIO_IdleFlags *idle_flags0)
{
	return OpenEXR_IdleHook(basic_dataP, idle_flags0);
}


A_Err
FrameSeq_PurgeHook(const SPBasicSuite *pica_basicP)
{
	return OpenEXR_PurgeHook(pica_basicP);
}


A_Err
FrameSeq_ConstructModuleInfo(
	AEIO_ModuleInfo	*info)
{
	// tell AE all about our capabilities
	return OpenEXR_ConstructModuleInfo(info);
}

#pragma mark-

A_Err	
FrameSeq_VerifyFileImportable(
	AEIO_BasicData			*basic_dataP,
	AEIO_ModuleSignature	sig, 
	const A_PathType		*file_pathZ, 
	A_Boolean				*importablePB)
{ 
	// quick check to see if file is really in our format
#ifdef AE_HFS_PATHS	
	A_char				pathZ[AEGP_MAX_PATH_SIZE];

	// convert the path format
	if(A_Err_NONE != ConvertPath(file_pathZ, pathZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME;
#else
	const A_PathType *pathZ = file_pathZ;
#endif

	return OpenEXR_VerifyFile(pathZ, importablePB);
}		


A_Err	
FrameSeq_GetInSpecInfo(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH, 
	AEIO_Verbiage	*verbiageP)
{ 
	// all this does it print the info (verbiage) about the file in the project window
	// you can also mess with file name and type if you must
	
	A_Err err			=	A_Err_NONE;
	
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	A_PathType			file_nameZ[AEGP_MAX_PATH_SIZE] = {'\0'};
	
	AEIO_Handle		optionsH		=	NULL;
	format_inData	*options			=	NULL;


	// get file path (or not - this can cause errors if the file lived on a drive that's been unmounted)
	//err = suites.IOInSuite2()->AEGP_GetInSpecFilePath(specH, file_nameZ);


	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);

	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
		
	
	// initialize the verbiage
	verbiageP->type[0] = '\0';
	verbiageP->sub_type[0] = '\0';
	
	
	err = OpenEXR_GetInSpecInfo(file_nameZ, options, verbiageP);
	
	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	
	return err;
}


A_Err	
FrameSeq_InitInSpecFromFile(
	AEIO_BasicData		*basic_dataP,
	const A_PathType	*file_pathZ,
	AEIO_InSpecH		specH)
{ 
	// tell AE all the necessary information
	// we pass our format function a nice, easy struct to populate and then deal with it
	
	A_Err ae_err					=	A_Err_NONE;

	AEIO_Handle		optionsH		=	NULL,
					old_optionsH	=	NULL;
	format_inData	*options			=	NULL;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);
	
	try{
	
	ErrThrower err;

	A_long			file_size = 0;
	

	// fill out the data structure for our function
	PF_Pixel 	premult_color = {255, 0, 0, 0};
	FIEL_Label	field_label;

	FrameSeq_Info info; // this is what we must tell AE about a frame
	
	Time_Code time_code;
	A_Chromaticities chrm;
	
#ifdef AE_HFS_PATHS	
	A_char		pathZ[AEGP_MAX_PATH_SIZE];

	// convert the path format
	if(A_Err_NONE != ConvertPath(file_pathZ, pathZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME;
#else
	const A_PathType *pathZ = file_pathZ;
#endif

	// fill in the file size
	GetFileSize(pathZ, &file_size);
	
	
	AEFX_CLR_STRUCT(field_label);
	field_label.order		=	FIEL_Order_LOWER_FIRST;
	field_label.type		=	FIEL_Type_UNKNOWN;
	field_label.version		=	FIEL_Label_VERSION;
	field_label.signature	=	FIEL_Tag;
	
	
	info.width			= 0;
	info.height			= 0;
	info.planes			= 0;
	info.depth			= 8;
	info.alpha_type		= AEIO_Alpha_UNKNOWN;
	info.premult_color	= &premult_color;
	info.field_label	= &field_label;
	info.pixel_aspect_ratio.num = info.pixel_aspect_ratio.den = 0;
	info.framerate.num = 0;
	info.framerate.den = 0;
	info.time_code = NULL;

	info.icc_profile = NULL;
	info.icc_profile_len = 0;
	
	time_code.hours = 0;
	time_code.minutes = 0;
	time_code.seconds = 0;
	time_code.frame = 0;
	time_code.drop_frame = FALSE;
	time_code.negative = FALSE;
	time_code.tc_set = FALSE;
	time_code.str[0] = '\0';

	Init_Chromaticities(&chrm);
	info.chromaticities = &chrm;
	
	info.render_info = NULL;
	
	
	// set up options data
	err = suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Input Options",
													sizeof(format_inData),
													AEGP_MemFlag_CLEAR, &optionsH);

	// assign the handle, look for old options
	err = suites.IOInSuite()->AEGP_SetInSpecOptionsHandle(specH, (void*)optionsH, (void**)&old_optionsH);

	err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
	
	// set input defaults
	OpenEXR_InitInOptions(basic_dataP, options);

	if(old_optionsH)
	{
		format_inData	*new_options = options;
		format_inData	*old_options = NULL;
		
		err = suites.MemorySuite()->AEGP_LockMemHandle(old_optionsH, (void**)&old_options);
		
		OpenEXR_CopyInOptions(basic_dataP, new_options, old_options);
		
		err = suites.MemorySuite()->AEGP_UnlockMemHandle(old_optionsH);
	}
	
	if(gDriverImplVersionMajor >= 117) // can accept timecode information (earlier versions would lock the interpretation dialog)
		info.time_code = &time_code;
	
	
	// run the function
	err = OpenEXR_FileInfo(basic_dataP, pathZ, &info, optionsH);

	
	// communicate info to AE
	A_Time			dur;
	A_Boolean		has_alpha;
		
	A_short			depth = info.planes < 3 ? (info.depth == 32 ? AEIO_InputDepth_GRAY_32 : (info.depth == 16 ? AEIO_InputDepth_GRAY_16 : AEIO_InputDepth_GRAY_8)) : // greyscale
							info.planes * info.depth; // not

	err = suites.IOInSuite()->AEGP_SetInSpecDepth(specH, depth);

	err = suites.IOInSuite()->AEGP_SetInSpecDimensions(specH, info.width, info.height);
	
	// have to set duration.scale or AE complains
	dur.value = 0;
	dur.scale = 100;
	err = suites.IOInSuite()->AEGP_SetInSpecDuration(specH, &dur);
	
#ifdef AE170_TIMECODE_SUITES
	if(gDriverImplVersionMajor >= 117 && info.framerate.num > 0 && info.framerate.den > 0)
	{
		// AE uses some unorthodox frame rate ratios
		if(info.framerate.den == 1001)
		{
			if(info.framerate.num == 24000)
			{
				info.framerate.num = 2997;
				info.framerate.den = 125;
			}
			else if(info.framerate.num == 30000)
			{
				info.framerate.num = 2997;
				info.framerate.den = 100;
			}
			else if(info.framerate.num == 60000)
			{
				info.framerate.num = 2997;
				info.framerate.den = 50;
			}
		}
		
		A_Fixed framerate = (((A_u_longlong)info.framerate.num * 65536) + (info.framerate.den / 2)) / info.framerate.den;
		
		err = suites.IOInSuite5()->AEGP_SetInSpecStillSequenceNativeFPS(specH, framerate);
		
		
		if(info.time_code != NULL && info.time_code->str[0] != '\0')
		{
			if(!info.time_code->tc_set)
			{
				if( ParseTimeCodeString(info.time_code->str, info.time_code) )
				{
					info.time_code->tc_set = TRUE;
				}
			}
		#ifndef NDEBUG
			else
			{
				Time_Code tempTimeCode;
				
				if( ParseTimeCodeString(info.time_code->str, &tempTimeCode) )
				{
					assert(tempTimeCode.hours == info.time_code->hours);
					assert(tempTimeCode.minutes == info.time_code->minutes);
					assert(tempTimeCode.seconds == info.time_code->seconds);
					assert(tempTimeCode.frame == info.time_code->frame);
					assert(tempTimeCode.drop_frame == info.time_code->drop_frame);
					assert(tempTimeCode.negative == info.time_code->negative);
				}
				else
					assert(FALSE);
			}
		#endif
		}
		
		if(info.time_code != NULL && info.time_code->tc_set)
		{
			A_Time start_time;
			
			CalculateStartTime(info.time_code, &info.framerate, &start_time);
			
			err = suites.IOInSuite5()->AEGP_SetInSpecNativeStartTime(specH, &start_time);
			
			err = suites.IOInSuite5()->AEGP_SetInSpecNativeDisplayDropFrame(specH, info.time_code->drop_frame);
		}
	}
#endif


	has_alpha = ( (info.planes == 4) || (info.planes == 2) ) &&
					(info.alpha_type != AEIO_Alpha_NONE);

	if(info.alpha_type != AEIO_Alpha_UNKNOWN)
	{
		AEIO_AlphaLabel	alpha;
		AEFX_CLR_STRUCT(alpha);

		alpha.alpha		=	has_alpha ? info.alpha_type : AEIO_Alpha_NONE;
		alpha.flags		=	(alpha.alpha == AEIO_Alpha_PREMUL) ? AEIO_AlphaPremul : 0;
		alpha.version	=	AEIO_AlphaLabel_VERSION;
		alpha.red		=	premult_color.red;
		alpha.green		=	premult_color.green;
		alpha.blue		=	premult_color.blue;
		
		suites.IOInSuite()->AEGP_SetInSpecAlphaLabel(specH, &alpha);
	}
	
	if(field_label.type != FIEL_Type_UNKNOWN)
	{
		suites.IOInSuite()->AEGP_SetInSpecInterlaceLabel(specH, &field_label);
	}
	
	if(info.pixel_aspect_ratio.num != 0 && info.pixel_aspect_ratio.den != 0)
	{
		suites.IOInSuite()->AEGP_SetInSpecHSF(specH, &info.pixel_aspect_ratio);
	}
	
	if(file_size != 0)
	{
		suites.IOInSuite()->AEGP_SetInSpecSize(specH, file_size);
	}
	
	if( (info.icc_profile && info.icc_profile_len) || Chromaticities_Set(info.chromaticities))
	{
		if(info.icc_profile == NULL || info.icc_profile_len == 0)
		{
			Chromaticities_To_Profile(&info.icc_profile, &info.icc_profile_len, info.chromaticities);
		}
		
		if(info.icc_profile && info.icc_profile_len)
		{
			AEGP_ColorProfileP ae_color_profile = NULL;
			
			suites.ColorSettingsSuite()->AEGP_GetNewColorProfileFromICCProfile(S_mem_id,
																				info.icc_profile_len, info.icc_profile,
																				&ae_color_profile);
			
			suites.IOInSuite()->AEGP_SetInSpecEmbeddedColorProfile(specH, ae_color_profile, NULL);
			
			// AE made a copy, so now we disopse?
			suites.ColorSettingsSuite()->AEGP_DisposeColorProfile(ae_color_profile);
			
			// free the profile
			free(info.icc_profile);
		}
	}
		
	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }

	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	
	// get rid of the options handle because we got an error
	if(ae_err && optionsH)
		suites.MemorySuite()->AEGP_FreeMemHandle(optionsH);


	return ae_err;
}


A_Err	
FrameSeq_DrawSparseFrame(
	AEIO_BasicData					*basic_dataP,
	AEIO_InSpecH					specH, 
	const AEIO_DrawSparseFramePB	*sparse_framePPB, 
	PF_EffectWorld					*wP,
	AEIO_DrawingFlags				*draw_flagsP)
{ 
	// we'll give a file-size buffer for filling and then fit it
	// to what AE wants
	
	A_Err ae_err					=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);


#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
#endif
	AEIO_Handle		optionsH		=	NULL;

	PF_EffectWorld	temp_World_data;
	
	PF_EffectWorld	*temp_World = &temp_World_data;

	try{
	
	ErrThrower err;

	format_inData	*options			=	NULL;

	
	// file path
#ifdef AE_UNICODE_PATHS
	A_PathType *file_nameZ = NULL;
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, &pathH);
	
	if(pathH)
	{
		err = suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_nameZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType				file_nameZ[AEGP_MAX_PATH_SIZE];
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, file_nameZ);
#endif
	
#ifdef AE_HFS_PATHS	
	// convert the path format
	if(A_Err_NONE != ConvertPath(file_nameZ, file_nameZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME; 
#endif

	// fill out the data structure for our function
	// should match what we previously filled
	// although this time we're asking AE for the info
	FrameSeq_Info info;
	
	A_long width, height;
	A_short depth;

	PF_Pixel 	premult_color = {0, 0, 0, 255};
	FIEL_Label	field_label;

	AEIO_AlphaLabel alphaL;
	
	// get all that info from AE
	err = suites.IOInSuite()->AEGP_GetInSpecDimensions(specH, &width, &height);
	err = suites.IOInSuite()->AEGP_GetInSpecDepth(specH, &depth);
	err = suites.IOInSuite()->AEGP_GetInSpecAlphaLabel(specH, &alphaL);
	err = suites.IOInSuite()->AEGP_GetInSpecInterlaceLabel(specH, &field_label);
	
	// set the info struct
	info.width = width;
	info.height = height;
	info.planes = (depth == 32 || depth == 64 || depth == 128) ? 4 : (depth == AEIO_InputDepth_GRAY_8 || depth == AEIO_InputDepth_GRAY_16 || depth == AEIO_InputDepth_GRAY_32) ? 1 : 3;
	info.depth  = (depth == 48 || depth == 64 || depth == AEIO_InputDepth_GRAY_16) ? 16 : (depth == 96 || depth == 128 || depth == AEIO_InputDepth_GRAY_32) ? 32 : 8;
	
	info.alpha_type = alphaL.alpha;
	premult_color.red = alphaL.red;
	premult_color.green = alphaL.green;
	premult_color.blue = alphaL.blue;
	info.premult_color = &premult_color;
	
	info.field_label = &field_label;

	
	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);


	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
	
	
	PF_EffectWorld	*active_World = NULL;
	
	PF_PixelFormat	pixel_format;

	err = suites.PFWorldSuite()->PF_GetPixelFormat(wP, &pixel_format);

	A_long		wP_depth =	(pixel_format == PF_PixelFormat_ARGB128)? 32 :
							(pixel_format == PF_PixelFormat_ARGB64) ? 16 : 8;
	

	// here's the only time we won't need to make our own buffer
	if(	(info.width == wP->width) && (info.height == wP->height) && (wP_depth == 32) )
	{
		active_World = wP; // just use the PF_EffectWorld AE gave us
		
		temp_World = NULL; // won't be needing this
	}
	else
	{
		assert(wP_depth == 32); // AE shouldn't be asking me to fill an integer buffer
		
		// make our own PF_EffectWorld
		err = suites.PFWorldSuite()->PF_NewWorld(NULL, info.width, info.height, FALSE,
												PF_PixelFormat_ARGB128, temp_World);
		
		active_World = temp_World;
	}


	// should always pass a full-sized float world to write into (using options we pass)
	ae_err = OpenEXR_DrawSparseFrame(basic_dataP, sparse_framePPB, active_World,
									draw_flagsP, file_nameZ, &info, options);

	

	if(temp_World && !ae_err)
	{
		err = SmartCopyWorld(basic_dataP, temp_World, wP, FALSE, FALSE, TRUE);
	}

	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }

	if(temp_World)
		suites.PFWorldSuite()->PF_DisposeWorld(NULL, temp_World);

	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);

#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	return ae_err;
}


A_Err	
FrameSeq_SeqOptionsDlg(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH,  
	A_Boolean		*user_interactedPB0)
{
	A_Err err						=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	AEIO_Handle		optionsH		=	NULL;
	format_inData	*options			=	NULL;
	
	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);

	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
	

	if(!err && options)
	{
		// pop up a dialog and change those read options
		err = OpenEXR_ReadOptionsDialog(basic_dataP, options, user_interactedPB0);
	}


	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	

	return err;
}


A_Err
FrameSeq_DisposeInSpec(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH)
{ 
	// dispose input options
	
	A_Err				err			=	A_Err_NONE; 
	AEIO_Handle			optionsH	=	NULL;
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);	

	// I guess we'll dump the handle if we've got it
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, reinterpret_cast<void**>(&optionsH));

	if(!err && optionsH)
	{
		err = suites.MemorySuite()->AEGP_FreeMemHandle(optionsH);
	}

	return err;
}


A_Err
FrameSeq_FlattenOptions(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH,
	AEIO_Handle		*flat_optionsPH)
{ 
	// pass a new handle of flattened options for saving
	// but no, don't delete the handle we're using
	
	A_Err				err			=	A_Err_NONE; 
	AEIO_Handle			optionsH	=	NULL;
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);	

	
	// get the options for flattening
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, reinterpret_cast<void**>(&optionsH));

	if(!err && optionsH)
	{
		AEGP_MemSize mem_size;
		
		void *round_data, *flat_data;
		
		// make a new handle that's the same size
		suites.MemorySuite()->AEGP_GetMemHandleSize(optionsH, &mem_size);
		
		suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Flat Options",
												mem_size,
												AEGP_MemFlag_CLEAR, flat_optionsPH);
		
		suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&round_data);
		suites.MemorySuite()->AEGP_LockMemHandle(*flat_optionsPH, (void**)&flat_data);
		
		// copy data
		memcpy((char*)flat_data, (char*)round_data, mem_size);
		
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
		suites.MemorySuite()->AEGP_UnlockMemHandle(*flat_optionsPH);
		
		// fun our flatten function
		OpenEXR_FlattenInputOptions(basic_dataP, *flat_optionsPH);
		
		// just because we're flattening the options doesn't mean we're done with them
		//suites.MemorySuite()->AEGP_FreeMemHandle(optionsH);
	}
	
	return err;
}		

A_Err
FrameSeq_InflateOptions(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH,
	AEIO_Handle		flat_optionsH)
{ 
	// take flat options handle and then set a new inflated options handle
	// AE wants to take care of the flat one for you this time, so no delete
	A_Err				err			=	A_Err_NONE; 
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);	

	
#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
	A_char				file_pathZ[AEGP_MAX_PATH_SIZE];
	
	// file path
	suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, file_pathZ);
#else
	AEGP_MemHandle	pathH = NULL;
	A_PathType		*file_pathZ = NULL;
	
	suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, &pathH);
	
	if(pathH)
	{
		suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_pathZ);
	}
#endif
	
	if(flat_optionsH)
	{
		AEGP_MemSize	mem_size;
		AEIO_Handle		optionsH	=	NULL,
						old_optionsH =	NULL;
		
		void *round_data, *flat_data;
		
		// make a new handle that's the same size
		suites.MemorySuite()->AEGP_GetMemHandleSize(flat_optionsH, &mem_size);

		suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Round Options",
												mem_size,
												AEGP_MemFlag_CLEAR, &optionsH);
		
		suites.MemorySuite()->AEGP_LockMemHandle(flat_optionsH, (void**)&flat_data);
		suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&round_data);
		
		// copy it
		memcpy((char*)round_data, (char*)flat_data, mem_size);
		
		suites.MemorySuite()->AEGP_UnlockMemHandle(flat_optionsH);
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
		
		// inflate copied data
		if(true) // file_pathZ && file_pathZ[0] != '\0') // turns out we get a bogus path here for sequences, I'd call that a bug
		{
			OpenEXR_InflateInputOptions(basic_dataP, optionsH);
		}

		suites.IOInSuite()->AEGP_SetInSpecOptionsHandle(specH, (void*)optionsH, (void**)&old_optionsH);

		// we'll let AE get rid of the flat options handle for us
		//suites.MemorySuite()->AEGP_FreeMemHandle(flat_optionsH);
	}


#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	return err;
}		

#pragma mark-

A_Err	
FrameSeq_GetNumAuxChannels(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH,
	A_long			*num_channelsPL)
{ 
	// tell AE how many Aux channels we have (with options help)
	A_Err ae_err					=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
#endif

	AEIO_Handle		optionsH		=	NULL;
	
	try{
	
	ErrThrower err;

	// file path
#ifdef AE_UNICODE_PATHS
	A_PathType *file_nameZ = NULL;
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, &pathH);
	
	if(pathH)
	{
		err = suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_nameZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType				file_nameZ[AEGP_MAX_PATH_SIZE];
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, file_nameZ);
#endif

#ifdef AE_HFS_PATHS	
	// convert the path format
	if(A_Err_NONE != ConvertPath(file_nameZ, file_nameZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME; 
#endif


	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);

	assert(optionsH); // this is a bug in AE otherwise

	if(optionsH)
	{
		// read the number of Auxiliary channels
		err = OpenEXR_GetNumAuxChannels(basic_dataP, optionsH, file_nameZ, num_channelsPL);
	}
	else
	{
		// gotta do it with fake options
		AEIO_Handle		fake_optionsH	= NULL;
		format_inData	*fake_options	= NULL;
	
		err = suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Round Options",
														sizeof(format_inData),
														AEGP_MemFlag_CLEAR, &fake_optionsH);
															
		err = suites.MemorySuite()->AEGP_LockMemHandle(fake_optionsH, (void**)&fake_options);
		
		err = OpenEXR_InitInOptions(basic_dataP, (format_inData *)fake_options);
		
		err = OpenEXR_GetNumAuxChannels(basic_dataP, fake_optionsH, file_nameZ, num_channelsPL);
		
		suites.MemorySuite()->AEGP_FreeMemHandle(fake_optionsH);
	}
	
	
	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }

#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);

	return ae_err;
}

									
A_Err	
FrameSeq_GetAuxChannelDesc(	
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH	specH,
	A_long			chan_indexL,
	PF_ChannelDesc	*descP)
{ 
	// describe one of our AUX channels
	A_Err ae_err = A_Err_NONE;

	AEGP_SuiteHandler suites(basic_dataP->pica_basicP);

#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
#endif

	AEIO_Handle optionsH = NULL;

	try{
	
	ErrThrower err;

	// file path
#ifdef AE_UNICODE_PATHS
	A_PathType *file_nameZ = NULL;
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, &pathH);
	
	if(pathH)
	{
		err = suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_nameZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType				file_nameZ[AEGP_MAX_PATH_SIZE];
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, file_nameZ);
#endif

#ifdef AE_HFS_PATHS	
	// convert the path format
	if(A_Err_NONE != ConvertPath(file_nameZ, file_nameZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME; 
#endif

	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);

	assert(optionsH); // this is a bug in AE otherwise

	if(optionsH)
	{
		// get the channel description
		err = OpenEXR_GetAuxChannelDesc(basic_dataP, optionsH, file_nameZ, chan_indexL, descP);
	}
	else
	{
		// gotta do it with fake options
		AEIO_Handle		fake_optionsH	= NULL;
		format_inData	*fake_options	= NULL;
		
		err = suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Round Options",
														sizeof(format_inData),
														AEGP_MemFlag_CLEAR, &fake_optionsH);
															
		err = suites.MemorySuite()->AEGP_LockMemHandle(fake_optionsH, (void**)&fake_options);
		
		err = OpenEXR_InitInOptions(basic_dataP, (format_inData *)fake_options);
		
		err = OpenEXR_GetAuxChannelDesc(basic_dataP, fake_optionsH, file_nameZ, chan_indexL, descP);
		
		suites.MemorySuite()->AEGP_FreeMemHandle(fake_optionsH);
	}


	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }

#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	
	return ae_err;
}

																
A_Err	
FrameSeq_DrawAuxChannel(
	AEIO_BasicData	*basic_dataP,
	AEIO_InSpecH			specH,
	A_long					chan_indexL,
	const AEIO_DrawFramePB	*pbP,
	PF_ChannelChunk			*chunkP)
{ 
	// read in the aux channel
	// unfortunately, you'll have to handle scaling yourself
	// I don't know how to scale float worlds for you easily
	
	A_Err ae_err					=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
#endif

	AEIO_Handle		optionsH		=	NULL;

	try{
	
	ErrThrower err;
	
	format_inData	*options			=	NULL;

		
	// file path
#ifdef AE_UNICODE_PATHS
	A_PathType *file_nameZ = NULL;
	
	err = suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, &pathH);
	
	if(pathH)
	{
		err = suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_nameZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType				file_nameZ[AEGP_MAX_PATH_SIZE];
	
	suites.IOInSuite()->AEGP_GetInSpecFilePath(specH, file_nameZ);
#endif
	
#ifdef AE_HFS_PATHS	
	// convert the path format
	if(A_Err_NONE != ConvertPath(file_nameZ, file_nameZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME; 
#endif

	
	A_long width, height;

	
	// get all that info from AE
	err = suites.IOInSuite()->AEGP_GetInSpecDimensions(specH, &width, &height);
	
	
	// get options handle
	err = suites.IOInSuite()->AEGP_GetInSpecOptionsHandle(specH, (void**)&optionsH);

	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
	else
		return AEIO_Err_INCONSISTENT_PARAMETERS; // we need options

	
	// use our own function to get the ChannelDesc (good thinking!)
	PF_ChannelDesc desc;
	err = FrameSeq_GetAuxChannelDesc(basic_dataP, specH, chan_indexL, &desc);

	
	// crap, we have to scale ourselves (for a channel like object ID, antialiasing makes no sense)
	PF_Point scale;
	scale.h = pbP->rs.x.den / pbP->rs.x.num; // scale.h = 2 means 1/2 x scale
	scale.v = pbP->rs.y.den / pbP->rs.y.num;
	
	
	// setup the Channel Chunk and allocate memory
	chunkP->widthL = ceil( (float)width / (float)scale.h );
	chunkP->heightL = ceil( (float)height / (float)scale.v );
	chunkP->dimensionL = desc.dimension;
	
	chunkP->data_type = desc.data_type;


	// pretty table of the various sizeof()'s
	A_u_char bytes_per_pixel = (	chunkP->data_type == PF_DataType_FLOAT			? 4 :
									chunkP->data_type == PF_DataType_DOUBLE			? 8 :
									chunkP->data_type == PF_DataType_LONG			? 4 :
									chunkP->data_type == PF_DataType_SHORT			? 2 :
									chunkP->data_type == PF_DataType_FIXED_16_16	? 4 :
									chunkP->data_type == PF_DataType_CHAR			? 1 :
									chunkP->data_type == PF_DataType_U_BYTE			? 1 :
									chunkP->data_type == PF_DataType_U_SHORT		? 2 :
									chunkP->data_type == PF_DataType_U_FIXED_16_16	? 4 :
									chunkP->data_type == PF_DataType_RGB			? 3 :
									4); // else?


	chunkP->row_bytesL = chunkP->widthL * chunkP->dimensionL * bytes_per_pixel;
	
	
	// in other words, we'll need
	AEGP_MemSize mem_size = chunkP->row_bytesL * chunkP->heightL;
	
	
	// for PF_Handles we use HandleSuite!
	chunkP->dataH = suites.HandleSuite()->host_new_handle(mem_size);
	
	chunkP->dataPV = suites.HandleSuite()->host_lock_handle(chunkP->dataH);
	
	memset(chunkP->dataPV, 0, mem_size);
	

	ae_err = OpenEXR_DrawAuxChannel(basic_dataP, options, file_nameZ, chan_indexL, pbP, scale, chunkP);

	
	suites.HandleSuite()->host_unlock_handle(chunkP->dataH);
	
	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }
	
#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
		
	return ae_err;
}


A_Err	
FrameSeq_FreeAuxChannel(	
	AEIO_BasicData		*basic_dataP,
	AEIO_InSpecH		specH,
	PF_ChannelChunk		*chunkP)
{ 
	A_Err err	=	A_Err_NONE;
	
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	if(chunkP->dataH)
	{
		// yep, PF_Handles
		suites.HandleSuite()->host_unlock_handle(chunkP->dataH);
		
		suites.HandleSuite()->host_dispose_handle(chunkP->dataH); // run free!
	}
	
	return err; 
}


//     input
// =====================================================================
//     output

#pragma mark-


A_Err	
FrameSeq_GetDepths(
	AEIO_BasicData			*basic_dataP,
	AEIO_OutSpecH			outH, 
	AEIO_SupportedDepthFlags	*which)
{
	// just tell us what depths to enable in the menu
	return OpenEXR_GetDepths(which);
}


A_Err	
FrameSeq_InitOutputSpec(
	AEIO_BasicData			*basic_dataP,
	AEIO_OutSpecH			outH, 
	A_Boolean				*user_interacted)
{
	// pass a new handle with output options
	// you may have to initialize the data, but you probably have
	// an old options handle to read from (and then dispose actually)
	
	A_Err err						=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	AEIO_Handle		optionsH		=	NULL,
					old_optionsH	=	NULL,
					old_old_optionsH=	NULL;
	format_outData	*options			=	NULL,
					*old_options		=	NULL;


	AEGP_MemSize mem_size;

	// make new options handle
	suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Output Options",
											sizeof(format_outData),
											AEGP_MemFlag_CLEAR, &optionsH);
	
	err = suites.MemorySuite()->AEGP_GetMemHandleSize(optionsH, &mem_size);
	
	err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);

	
	// get old options
	suites.IOOutSuite()->AEGP_GetOutSpecOptionsHandle(outH, (void**)&old_optionsH);
	
	
	if(old_optionsH)
	{
		AEGP_MemSize old_size;
		
		err = suites.MemorySuite()->AEGP_GetMemHandleSize(old_optionsH, &old_size);
		
		if(old_size < mem_size)
		{
			suites.MemorySuite()->AEGP_UnlockMemHandle(old_optionsH);
			suites.MemorySuite()->AEGP_ResizeMemHandle("Old Handle Resize", mem_size, old_optionsH);
		}
		
		suites.MemorySuite()->AEGP_LockMemHandle(old_optionsH, (void**)&old_options);
		
		// first we copy the data
		memcpy((char*)options, (char*)old_options, mem_size);
		
		// then we inflate it
		OpenEXR_InflateOutputOptions((format_outData *)options);

		suites.MemorySuite()->AEGP_UnlockMemHandle(old_optionsH);
	}
	else
	{
		err = OpenEXR_InitOutOptions(options);
	}

	suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	
	
	// set the options handle
	suites.IOOutSuite()->AEGP_SetOutSpecOptionsHandle(outH, (void*)optionsH, (void**)&old_old_optionsH);
	
	
	// so now AE wants me to delete this. whatever.
	if(old_old_optionsH)
		suites.MemorySuite()->AEGP_FreeMemHandle(old_old_optionsH);

	
	return err;
}


A_Err	
FrameSeq_OutputFrame(
	AEIO_BasicData	*basic_dataP,
	AEIO_OutSpecH			outH, 
	const PF_EffectWorld	*wP)
{
	// write that frame
	// again, we'll get the info and provide a suitable buffer
	// you just write the file, big guy
	
	A_Err ae_err = A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
#endif

	AEIO_Handle			optionsH		=	NULL;

	PF_EffectWorld		temp_World_data;

	PF_EffectWorld		*temp_World		=	&temp_World_data,
						*active_World	=	NULL;

	try{
	
	ErrThrower err;

	format_outData		*options		=	NULL;
	
	PF_Pixel 			premult_color = {0, 0, 0, 255};
	AEIO_AlphaLabel		alpha;
	FIEL_Label			field;
	
	Time_Code			time_code;
	A_Chromaticities	chrom;
	
	Render_Info			render_info;
	
	A_short				depth;
	
	FrameSeq_Info		info;
	
	
	// get options data
	err = suites.IOOutSuite()->AEGP_GetOutSpecOptionsHandle(outH, (void**)&optionsH);
	
	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
	
	
	// get file path
#ifdef AE_UNICODE_PATHS
	A_PathType *file_pathZ = NULL;
	
	A_Boolean file_reservedPB = FALSE; // WTF?
	err = suites.IOOutSuite()->AEGP_GetOutSpecFilePath(outH, &pathH, &file_reservedPB);
	
	if(pathH)
	{
		err = suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_pathZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType file_pathZ[AEGP_MAX_PATH_SIZE+1];
	
	A_Boolean file_reservedPB = FALSE; // WTF?
	err = suites.IOOutSuite()->AEGP_GetOutSpecFilePath(outH, file_pathZ, &file_reservedPB);
#endif
	
#ifdef AE_HFS_PATHS	
	// convert the path format
	if(A_Err_NONE != ConvertPath(file_pathZ, file_pathZ, AEGP_MAX_PATH_SIZE-1) )
		return AEIO_Err_BAD_FILENAME; 
#endif
	
	// get dimensions
	err = suites.IOOutSuite()->AEGP_GetOutSpecDimensions(outH, &info.width, &info.height);
	
	
	// get depth
	err = suites.IOOutSuite()->AEGP_GetOutSpecDepth(outH, &depth);

	// translate to planes & depth - negative depths are greyscale
	info.planes = (depth == 32 || depth == 64 || depth == 128) ?  4 : (depth == AEIO_InputDepth_GRAY_8 || depth == AEIO_InputDepth_GRAY_16 || depth == AEIO_InputDepth_GRAY_32) ? 1 : 3;
	info.depth  = (depth == AEIO_InputDepth_GRAY_16 || depth == 48 || depth == 64) ? 16 : (depth == 96 || depth == 128 || depth == AEIO_InputDepth_GRAY_32) ? 32 : 8;
	
	
	// get pixel aspect ratio
	err = suites.IOOutSuite()->AEGP_GetOutSpecHSF(outH, &info.pixel_aspect_ratio);

	
	// get frame rate
	A_Fixed framerate = 0;
	err = suites.IOOutSuite()->AEGP_GetOutSpecFPS(outH, &framerate);
	
	if(framerate % 65536 == 0)
	{
		info.framerate.num = framerate / 65536;
		info.framerate.den = 1;
	}
	else
	{
		info.framerate.num = (((long long)framerate * 1001) + 32768) / 65536;
		info.framerate.den = 1001;
	}
	
	
	// timecode
	info.time_code = NULL;
	
#ifdef AE170_TIMECODE_SUITES
	if(gDriverImplVersionMajor >= 117)
	{
		try
		{
			ErrThrower err2;
			
			A_Time start_time;
			err2 = suites.IOOutSuite5()->AEGP_GetOutSpecStartTime(outH, &start_time);
			
			A_Time frame_time;
			err2 = suites.IOOutSuite5()->AEGP_GetOutSpecFrameTime(outH, &frame_time);
			
			A_Boolean drop_frame = FALSE;
			
			if(info.framerate.den == 1001 && (info.framerate.num == 30000 || info.framerate.num == 60000))
				err2 = suites.IOOutSuite5()->AEGP_GetOutSpecIsDropFrame(outH, &drop_frame);
			
			const A_long frame_num = CalculateFrameNumber(start_time, frame_time, info.framerate);
			
			A_u_long frames_per_second = ((double)info.framerate.num / (double)info.framerate.den) + 0.5;
			
			CalculateTimeCode(frame_num, frames_per_second, drop_frame, &time_code);
			
			TimeCodeString(&time_code, time_code.str);
			
			info.time_code = &time_code;
			
		#ifndef NDEBUG
			// Checking my time code stuff
			Time_Code parsedTimeCode;
			
			if( ParseTimeCodeString(time_code.str, &parsedTimeCode) )
			{
				assert(parsedTimeCode.frame == time_code.frame);
				assert(parsedTimeCode.seconds == time_code.seconds);
				assert(parsedTimeCode.minutes == time_code.minutes);
				assert(parsedTimeCode.hours == time_code.hours);
				assert(parsedTimeCode.drop_frame == time_code.drop_frame);
				assert(parsedTimeCode.negative == time_code.negative);
				
				
				A_Ratio frame_rate;
				
				frame_rate.num = info.framerate.num;
				frame_rate.den = info.framerate.den;
				
				// AE frame rates
				if(frame_rate.den == 1001)
				{
					if(frame_rate.num == 24000)
					{
						frame_rate.num = 2997;
						frame_rate.den = 125;
					}
					else if(frame_rate.num == 30000)
					{
						frame_rate.num = 2997;
						frame_rate.den = 100;
					}
					else if(frame_rate.num == 60000)
					{
						frame_rate.num = 2997;
						frame_rate.den = 50;
					}
				}

				A_Time parsedStartTime;
				
				CalculateStartTime(&parsedTimeCode, &frame_rate, &parsedStartTime);
				
				assert(parsedStartTime.scale == start_time.scale && frame_time.scale == start_time.scale); // may not always be true
				assert(parsedStartTime.value == (start_time.value + frame_time.value));
			}
			else
				assert(FALSE);
		#endif
		}
		catch(...) { assert(FALSE); }
	}
#endif
	
	// get alpha info
	suites.IOOutSuite()->AEGP_GetOutSpecAlphaLabel(outH, &alpha);
	
	info.alpha_type = alpha.alpha;
	
	premult_color.red   = alpha.red;
	premult_color.green = alpha.green;
	premult_color.blue  = alpha.blue;
	info.premult_color = &premult_color;


	// get field info
	suites.IOOutSuite()->AEGP_GetOutSpecInterlaceLabel(outH, &field);
	
	info.field_label = &field;
	
	
	// ICC Profile
	AEGP_ColorProfileP ae_color_profile = NULL;
	AEGP_MemHandle icc_profileH = NULL;
	info.icc_profile = NULL;
	info.icc_profile_len = 0;
	info.chromaticities = NULL;
	
	A_Boolean embed_profile = FALSE;
	suites.IOOutSuite()->AEGP_GetOutSpecShouldEmbedICCProfile(outH, &embed_profile);
	
	if(embed_profile)
	{
		suites.IOOutSuite()->AEGP_GetNewOutSpecColorProfile(S_mem_id, outH, &ae_color_profile);
		
		if(ae_color_profile)
		{
			suites.ColorSettingsSuite()->AEGP_GetNewICCProfileFromColorProfile(S_mem_id, ae_color_profile, &icc_profileH);
			
			if(icc_profileH)
			{
				AEGP_MemSize mem_size;
				void *icc = NULL;
		
				suites.MemorySuite()->AEGP_GetMemHandleSize(icc_profileH, &mem_size);
				suites.MemorySuite()->AEGP_LockMemHandle(icc_profileH, (void**)&icc);
				
				if(icc)
				{
					info.icc_profile = icc;
					info.icc_profile_len = mem_size;
					
					Init_Chromaticities(&chrom);
					Profile_To_Chromaticities(info.icc_profile, info.icc_profile_len, &chrom);
					
					info.chromaticities = &chrom;
				}
			}
		}
	}
	
	

			
	// render info
	SetRenderInfo(basic_dataP, outH, &render_info);
	info.render_info = &render_info;
		
	
	// for EXR, we always want to pass a float buffer
	PF_PixelFormat	pixel_format;

	err = suites.PFWorldSuite()->PF_GetPixelFormat(wP, &pixel_format);

	if(pixel_format == PF_PixelFormat_ARGB128)
	{
		active_World = (PF_EffectWorld *)wP;
		
		temp_World = NULL;
	}
	else
	{
		// make our own float PF_EffectWorld
		err = suites.PFWorldSuite()->PF_NewWorld(NULL, info.width, info.height, FALSE,
												PF_PixelFormat_ARGB128, temp_World);
		
		// copy the world
		err = SmartCopyWorld(basic_dataP, (PF_EffectWorld *)wP, temp_World, FALSE, FALSE, FALSE);
			
		active_World = temp_World;
	}


	// write out image (finally)
	ae_err = OpenEXR_OutputFile(basic_dataP, file_pathZ, &info, options, active_World);

	

	// dispose color profile stuff
	if(icc_profileH)
		suites.MemorySuite()->AEGP_FreeMemHandle(icc_profileH);
	
	if(ae_color_profile)
		suites.ColorSettingsSuite()->AEGP_DisposeColorProfile(ae_color_profile);

	}
	catch(ErrThrower &err) { ae_err = err.err(); }
	catch(...) { ae_err = AEIO_Err_PARSING; }
	
	// dispose temp world if we made one	
	if(temp_World)
		suites.PFWorldSuite()->PF_DisposeWorld(NULL, temp_World);

	// dispose options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);

#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	return ae_err;
}


A_Err	
FrameSeq_UserOptionsDialog(
	AEIO_BasicData		*basic_dataP,
	AEIO_OutSpecH		outH, 
	const PF_EffectWorld	*sample0,
	A_Boolean			*user_interacted0)
{
	// output options dialog here
	// we'll give you the options data, you change it
	
	A_Err err						=	A_Err_NONE;

	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	AEIO_Handle			optionsH		=	NULL;
	format_outData	*options			=	NULL;
	
	// get options handle
	err = suites.IOOutSuite()->AEGP_GetOutSpecOptionsHandle(outH, (void**)&optionsH);

	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);


	if(!err && options)
	{
		// do a dialog and change those output options
		err = OpenEXR_WriteOptionsDialog(basic_dataP, options, user_interacted0);
	}


	// done with options
	if(optionsH)
		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	
	return err;
}


A_Err	
FrameSeq_GetOutputInfo(
	AEIO_BasicData		*basic_dataP,
	AEIO_OutSpecH		outH,
	AEIO_Verbiage		*verbiageP)
{ 
	// all this function does is print details about our output options
	// in the output module window
	// or rather, gets the options so your function can do that

	A_Err err			=	A_Err_NONE;
	
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);

	AEIO_Handle		optionsH		=	NULL;
	format_outData	*options			=	NULL;


	// get file path (but don't freak out if it's an invalid path (do you really need a path here?))
#ifdef AE_UNICODE_PATHS
	AEGP_MemHandle pathH = NULL;
	A_PathType *file_pathZ = NULL;
	
	A_Boolean file_reservedPB = FALSE; // WTF?
	suites.IOOutSuite()->AEGP_GetOutSpecFilePath(outH, &pathH, &file_reservedPB);
	
	if(pathH)
	{
		suites.MemorySuite()->AEGP_LockMemHandle(pathH, (void **)&file_pathZ);
	}
	else
		return AEIO_Err_BAD_FILENAME; 
#else
	A_PathType file_pathZ[AEGP_MAX_PATH_SIZE+1];
	
	A_Boolean file_reservedPB = FALSE; // WTF?
	suites.IOOutSuite()->AEGP_GetOutSpecFilePath(outH, file_pathZ, &file_reservedPB);
#endif
	
#ifdef AE_HFS_PATHS	
	// convert the path format
	ConvertPath(file_pathZ, file_pathZ, AEGP_MAX_PATH_SIZE-1);
#endif


	// get options handle
	err = suites.IOOutSuite()->AEGP_GetOutSpecOptionsHandle(outH, (void**)&optionsH);

	if(optionsH)
		err = suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&options);
		
	
	// initialize the verbiage
	verbiageP->type[0] = '\0';
	verbiageP->sub_type[0] = '\0';

	if(!err)
	{
		err = OpenEXR_GetOutSpecInfo(file_pathZ, options, verbiageP);
		
		// done with options
		if(optionsH)
			suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
	}

#ifdef AE_UNICODE_PATHS
	if(pathH)
		suites.MemorySuite()->AEGP_FreeMemHandle(pathH);
#endif

	return err;
}


A_Err	
FrameSeq_DisposeOutputOptions(
	AEIO_BasicData	*basic_dataP,
	void			*optionsPV) // what the...?
{ 
	// the options gotta go sometime
	// couldn't you just say optionsPV was a handle?
	
	A_Err				err			=	A_Err_NONE; 
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);	

	// here's our options handle apparently
	//AEIO_Handle		optionsH	=	reinterpret_cast<AEIO_Handle>(optionsPV);
	AEIO_Handle		optionsH	=	static_cast<AEIO_Handle>(optionsPV);
	
	if (!err && optionsH)
	{
		err = suites.MemorySuite()->AEGP_FreeMemHandle(optionsH);
	}
	return err;
}


A_Err	
FrameSeq_GetFlatOutputOptions(
	AEIO_BasicData	*basic_dataP,
	AEIO_OutSpecH	outH, 
	AEIO_Handle		*flat_optionsPH)
{
	// give AE and handle with flat options for saving
	// but don't delete the old handle, AE still wants it
	
	A_Err				err			=	A_Err_NONE; 
	AEIO_Handle			optionsH	=	NULL;
	AEGP_SuiteHandler	suites(basic_dataP->pica_basicP);	

	
	// get the options for flattening
	err = suites.IOOutSuite()->AEGP_GetOutSpecOptionsHandle(outH, reinterpret_cast<void**>(&optionsH));

	if(!err && optionsH)
	{
		AEGP_MemSize mem_size;
		
		format_outData *round_data, *flat_data;
		
		// make a new handle that's the same size
		// we're assuming that the options are already flat
		// although they may need byte flippage
		suites.MemorySuite()->AEGP_GetMemHandleSize(optionsH, &mem_size);
		
		suites.MemorySuite()->AEGP_NewMemHandle( S_mem_id, "Flat Options",
												mem_size,
												AEGP_MemFlag_CLEAR, flat_optionsPH);
		
		suites.MemorySuite()->AEGP_LockMemHandle(optionsH, (void**)&round_data);
		suites.MemorySuite()->AEGP_LockMemHandle(*flat_optionsPH, (void**)&flat_data);
		
		// first we copy
		memcpy((char*)flat_data, (char*)round_data, mem_size);
		
		// then we flatten
		OpenEXR_FlattenOutputOptions((format_outData *)flat_data);

		suites.MemorySuite()->AEGP_UnlockMemHandle(optionsH);
		suites.MemorySuite()->AEGP_UnlockMemHandle(*flat_optionsPH);
		
		// just because we're flattening the options doesn't mean we're done with them
		//suites.MemorySuite()->AEGP_FreeMemHandle(optionsH);
	}
	
	return err;
}

