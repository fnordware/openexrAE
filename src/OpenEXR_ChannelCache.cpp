//
//	OpenEXR file importer/exporter for After Effects (AEIO)
// 
//	by Brendan Bolles <brendan@fnordware.com>
//
//	see OpenEXR.cpp for more information
//

#include "OpenEXR_ChannelCache.h"

#include <half.h>
#include <ImfChannelList.h>
#include <IexBaseExc.h>

#include <IlmThread.h>
#include <IlmThreadPool.h>

#include <ImfVersion.h>
#include <ImfTileDescriptionAttribute.h>

#include <assert.h>

#include <vector>
#include <algorithm>


using namespace Imf;
using namespace Imath;
using namespace Iex;
using namespace IlmThread;
using namespace std;

extern AEGP_PluginID S_mem_id;


OpenEXR_HeaderCache::OpenEXR_HeaderCache(Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership, bool takeOwnership) :
	_parts(in->parts()),
	_header(in->header(0)),
	_channels(in->channels()),
	_dataWindow(in->dataWindow()),
	_displayWindow(in->displayWindow()),
	_path(stream->getPath()),
	_modtime(stream->getModTime()),
	_stream(NULL),
	_infile(NULL)
{
	if(takeOwnership)
	{
		_stream = stream;
		_infile = in;
		
		_stream->softClose();
		
		tookOwnership = true;
	}
	else
		tookOwnership = false;

	updateCacheTime();
}


OpenEXR_HeaderCache::~OpenEXR_HeaderCache()
{
	delete _infile;
	delete _stream;
}


void
OpenEXR_HeaderCache::updateCacheTime()
{
	_last_access = time(NULL);
}


double
OpenEXR_HeaderCache::cacheAge() const
{
	return difftime(time(NULL), _last_access);
}


bool
OpenEXR_HeaderCache::cacheIsStale(int timeout) const
{
	return (cacheAge() > timeout);
}


bool
OpenEXR_HeaderCache::takeObjects(Imf::HybridInputFile * &in, IStreamPlatform * &stream)
{
	if(_infile != NULL && _stream != NULL)
	{
		in = _infile;
		stream = _stream;
		
		_infile = NULL;
		_stream = NULL;
		
		return true;
	}
	else
		return false;
}


static void
FixSubsampling(const FrameBuffer &framebuffer, const Box2i &dw)
{
	for(FrameBuffer::ConstIterator i = framebuffer.begin(); i != framebuffer.end(); i++)
	{
		const Slice & slice = i.slice();
		
		if(slice.xSampling != 1 || slice.ySampling != 1)
		{
			char *row_subsampled = (char *)slice.base + (slice.yStride * (dw.min.y + ((1 + dw.max.y - dw.min.y) / slice.ySampling) - 1)) + (slice.xStride * (dw.min.x + ((1 + dw.max.x - dw.min.x) / slice.ySampling) - 1));
			char *row_expanded = (char *)slice.base + (slice.yStride * dw.max.y) + (slice.xStride * dw.max.x);
			
			for(int y = dw.max.y; y >= dw.min.y; y--)
			{
				char *pix_subsampled = row_subsampled;
				char *pix_expanded = row_expanded;
				
				for(int x = dw.max.x; x >= dw.min.x; x--)
				{
					if(slice.type == Imf::HALF)
					{
						*((half *)pix_expanded) = *((half *)pix_subsampled);
					}
					else if(slice.type == Imf::FLOAT)
					{
						*((float *)pix_expanded) = *((float *)pix_subsampled);
					}
					else if(slice.type == Imf::UINT)
					{
						*((unsigned int *)pix_expanded) = *((unsigned int *)pix_subsampled);
					}
					
					if(x % slice.xSampling == 0)
						pix_subsampled -= slice.xStride;
					
					pix_expanded -= slice.xStride;
				}
				
				if(y % slice.ySampling == 0)
					row_subsampled -= slice.yStride;
				
				row_expanded -= slice.yStride;
			}
		}
	}
}


class OpenEXR_ChannelCacheMemory::AllocateMemTask : public Task
{
  public:
	AllocateMemTask(TaskGroup *group, AEGP_MemorySuite *memSP,
					const string &name, const Channel &channel, const Box2i &dw,
					ChannelMap &cache, FrameBuffer &frameBuffer, Mutex &mutex);
	virtual ~AllocateMemTask() {}
	
	virtual void execute();
	
  private:
  	AEGP_MemorySuite *_memSP;
  	const string _name;
	const Channel _channel;
	const Box2i _dw;
	ChannelMap &_cache;
	FrameBuffer &_frameBuffer;
	Mutex &_mutex;
};

OpenEXR_ChannelCacheMemory::AllocateMemTask::AllocateMemTask(TaskGroup *group, AEGP_MemorySuite *memSP,
																const string &name, const Channel &channel, const Box2i &dw,
																ChannelMap &cache, FrameBuffer &frameBuffer, Mutex &mutex) :
	Task(group),
	_memSP(memSP),
	_name(name),
	_channel(channel),
	_dw(dw),
	_cache(cache),
	_frameBuffer(frameBuffer),
	_mutex(mutex)
{

}

void
OpenEXR_ChannelCacheMemory::AllocateMemTask::execute()
{
	const size_t pix_size =	_channel.type == Imf::HALF ? sizeof(half) :
							_channel.type == Imf::FLOAT ? sizeof(float) :
							_channel.type == Imf::UINT ? sizeof(unsigned int) :
							sizeof(float);

	const int width = _dw.max.x - _dw.min.x + 1;
	const int height = _dw.max.y - _dw.min.y + 1;

	const size_t rowbytes = pix_size * width;
	const size_t data_size = rowbytes * height;


	AEIO_Handle bufH = NULL;
	
	_memSP->AEGP_NewMemHandle(S_mem_id, "Channel Cache",
								data_size,
								AEGP_MemFlag_CLEAR, &bufH);

	if(bufH == NULL)
		throw NullExc("Can't allocate a channel cache handle like I need to.");


	char *buf = NULL;

	_memSP->AEGP_LockMemHandle(bufH, (void**)&buf);

	if(buf == NULL)
		throw NullExc("Why is the locked handle NULL?");


	{
		Lock lock(_mutex);
		
		_cache[ _name ] = ChannelCache(_channel.type, bufH);

		char * const channel_origin = buf - (pix_size * _dw.min.x) - (rowbytes * _dw.min.y);

		_frameBuffer.insert(_name, Slice(_channel.type,
											channel_origin,
											pix_size,
											rowbytes,
											_channel.xSampling,
											_channel.ySampling,
											0.0) );
	}
}


OpenEXR_ChannelCacheMemory::OpenEXR_ChannelCacheMemory(const SPBasicSuite *pica_basicP, const AEIO_InterruptFuncs *inter,
											HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership) :
	OpenEXR_ChannelCache(pica_basicP, inter, in, stream, tookOwnership, false),
	suites(pica_basicP)
{
	if(in == NULL || stream == NULL)
		throw NullExc("Unexpecedly NULL stream/file");
	
	const Box2i &dw = in->dataWindow();
	
	_width = dw.max.x - dw.min.x + 1;
	_height = dw.max.y - dw.min.y + 1;
	
	Mutex mutex;
	FrameBuffer frameBuffer;
	
	try
	{
		const ChannelList &channels = in->channels();
		
		{
			TaskGroup group;
			
			for(ChannelList::ConstIterator i = channels.begin(); i != channels.end(); ++i)
			{
				ThreadPool::addGlobalTask(new AllocateMemTask(&group,
																suites.MemorySuite(),
																i.name(),
																i.channel(),
																dw,
																_cache,
																frameBuffer,
																mutex) );
			}
		}
		
		in->setFrameBuffer(frameBuffer);
		
	
		A_Err err = A_Err_NONE;
	
	#ifdef NDEBUG
		#define CONT()	( (inter && inter->abort0) ? !(err = inter->abort0(inter->refcon) ) : TRUE)
		#define PROG(COUNT, TOTAL)	( (inter && inter->progress0) ? !(err = inter->progress0(inter->refcon, COUNT, TOTAL) ) : TRUE)
	#else
		#define CONT()					TRUE
		#define PROG(COUNT, TOTAL)		TRUE
	#endif
		
		if(in->isTiled())
		{
			const int begin_row = 0;
			const int end_row = (in->numYTiles() - 1);

			const int tiled_row_blocksize = TiledRowBlockSize(*in);

			int y = begin_row;

			while(y <= end_row && PROG(y - begin_row, 1 + end_row - begin_row) )
			{
				int high_row = min(y + tiled_row_blocksize - 1, end_row);
				
				in->readTileRows(y, high_row);
				
				y = high_row + 1;
			}
		}
		else
		{
			const int scanline_block_size = ScanlineBlockSize(*in);
		
			int y = dw.min.y;
			
			while(y <= dw.max.y && PROG(y - dw.min.y, dw.max.y - dw.min.y) )
			{
				int high_scanline = min(y + scanline_block_size - 1, dw.max.y);
				
				in->readPixels(y, high_scanline);
				
				y = high_scanline + 1;
			}
		}
		
		if(err)
			throw CancelExc(err);
	}
	catch(IoExc) {} // we catch these so that partial files are read partially without error
	catch(InputExc) {}
	catch(...)
	{
		// out of memory or worse; kill anything that got allocated
		for(ChannelMap::iterator i = _cache.begin(); i != _cache.end(); ++i)
		{
			if(i->second.bufH != NULL)
			{
				suites.MemorySuite()->AEGP_FreeMemHandle(i->second.bufH);
				
				i->second.bufH = NULL;
			}
		}
		
		updateCacheTime();
		
		throw; // re-throw the exception
	}
	
	FixSubsampling(frameBuffer, dw);
	
	for(ChannelMap::const_iterator i = _cache.begin(); i != _cache.end(); ++i)
	{
		if(i->second.bufH != NULL)
			suites.MemorySuite()->AEGP_UnlockMemHandle(i->second.bufH);
	}
	
	updateCacheTime();
}


class FreeMemTask : public Task
{
  public:
	FreeMemTask(TaskGroup *group, AEGP_MemorySuite *memSP, AEIO_Handle bufH);
	virtual ~FreeMemTask() {}
	
	virtual void execute();
	
  private:
  	AEGP_MemorySuite *_memSP;
	AEIO_Handle _bufH;
};

FreeMemTask::FreeMemTask(TaskGroup *group, AEGP_MemorySuite *memSP, AEIO_Handle bufH) :
	Task(group),
	_memSP(memSP),
	_bufH(bufH)
{

}

void
FreeMemTask::execute()
{
	_memSP->AEGP_FreeMemHandle(_bufH);
}


static TaskGroup gFreeMemGroup;

OpenEXR_ChannelCacheMemory::~OpenEXR_ChannelCacheMemory()
{
	for(ChannelMap::iterator i = _cache.begin(); i != _cache.end(); ++i)
	{
		if(i->second.bufH != NULL)
		{
			ThreadPool::addGlobalTask(new FreeMemTask(&gFreeMemGroup, suites.MemorySuite(), i->second.bufH) );
			
			i->second.bufH = NULL;
		}
	}
}


typedef struct CopyIterateData {
	const AEIO_InterruptFuncs *interP;
	const char *buf;
	int width;
	Imf::PixelType pix_type;
	const Slice &slice;
	const Box2i &dw;
	
	CopyIterateData(const AEIO_InterruptFuncs *i, const char *b, int w, Imf::PixelType p, const Slice &s, const Box2i &d) : interP(i), buf(b), width(w), pix_type(p), slice(s), dw(d) {}
} CopyIterateData;

template <typename INTYPE, typename OUTTYPE>
static void
CopyIterateRow(const char *in, char *out, size_t out_stride, int width)
{
	INTYPE *i = (INTYPE *)in;
	OUTTYPE *o = (OUTTYPE *)out;
	
	const int o_step = out_stride / sizeof(OUTTYPE);
	
	while(width--)
	{
		*o = *i++;
		
		o += o_step;
	}
}

static A_Err CopyIterate(void *refconPV,
							A_long thread_indexL,
							A_long i,
							A_long iterationsL)
{
	A_Err err = A_Err_NONE;
	
	CopyIterateData *i_data = (CopyIterateData *)refconPV;
	
	const Imf::PixelType pix_type = i_data->pix_type;
	const int width = i_data->width;
	const char *buf = i_data->buf;

	const size_t pix_size =	pix_type == Imf::HALF ? sizeof(half) :
							pix_type == Imf::FLOAT ? sizeof(float) :
							pix_type == Imf::UINT ? sizeof(unsigned int) :
							sizeof(float);
	
	const size_t rowbytes = pix_size * width;
	
	const char *in_row = buf + (rowbytes * i);
	
	const Slice &slice = i_data->slice;
	const Box2i &dw = i_data->dw;
	assert(width == dw.max.x - dw.min.x + 1);
	
	char *slice_row = slice.base + (slice.yStride * (dw.min.y + i)) + (slice.xStride * dw.min.x);
	
	if(pix_type == Imf::HALF)
	{
		if(slice.type == Imf::FLOAT)
			CopyIterateRow<half, float>(in_row, slice_row, slice.xStride, width);
		else if(slice.type == Imf::UINT)
			CopyIterateRow<half, unsigned int>(in_row, slice_row, slice.xStride, width);
	}
	else if(pix_type == Imf::FLOAT)
	{
		if(slice.type == Imf::FLOAT)
			CopyIterateRow<float, float>(in_row, slice_row, slice.xStride, width);
		else if(slice.type == Imf::UINT)
			CopyIterateRow<float, unsigned int>(in_row, slice_row, slice.xStride, width);
	}
	else if(pix_type == Imf::UINT)
	{
		if(slice.type == Imf::FLOAT)
			CopyIterateRow<unsigned int, float>(in_row, slice_row, slice.xStride, width);
		else if(slice.type == Imf::UINT)
			CopyIterateRow<unsigned int, unsigned int>(in_row, slice_row, slice.xStride, width);
	}

#ifdef NDEBUG
	if(thread_indexL == 0 && i_data->interP && i_data->interP->abort0)
		err = i_data->interP->abort0(i_data->interP->refcon);
#endif

	return err;
}


typedef struct FillIterateData {
	const AEIO_InterruptFuncs *interP;
	const Slice &slice;
	const Box2i &dw;
	
	FillIterateData(const AEIO_InterruptFuncs *i, const Slice &s, const Box2i &d) : interP(i), slice(s), dw(d) {}
} FillIterateData;

static A_Err FillIterate( void	*refconPV,
							A_long	thread_indexL,
							A_long	i,
							A_long	iterationsL)
{
	A_Err err = A_Err_NONE;
	
	FillIterateData *i_data = (FillIterateData *)refconPV;
	
	const Slice &slice = i_data->slice;
	const Box2i &dw = i_data->dw;
	
	char *slice_row = slice.base + (slice.yStride * (dw.min.y + i)) + (slice.xStride * dw.min.x);
	
	int width = dw.max.x - dw.min.x + 1;
	
	if(slice.type == Imf::FLOAT)
	{
		float *pix = (float *)slice_row;
		
		const int step = slice.xStride / sizeof(float);
		
		const float val = slice.fillValue;
		
		while(width--)
		{
			*pix = val;
			
			pix += step;
		}
	}
	else if(slice.type == Imf::UINT)
	{
		unsigned int *pix = (unsigned int *)slice_row;
		
		const int step = slice.xStride / sizeof(unsigned int);
		
		const unsigned int val = slice.fillValue;
		
		while(width--)
		{
			*pix = val;
			
			pix += step;
		}
	}
	

#ifdef NDEBUG
	if(thread_indexL == 0 && i_data->interP && i_data->interP->abort0)
		err = i_data->interP->abort0(i_data->interP->refcon);
#endif

	return err;
}


A_Err
OpenEXR_ChannelCacheMemory::fillFrameBuffer(const AEIO_InterruptFuncs *inter, const FrameBuffer &framebuffer, const Box2i &dw)
{
	A_Err err = A_Err_NONE;
	
	updateCacheTime();

	vector<AEIO_Handle> locked_handles;

	for(FrameBuffer::ConstIterator i = framebuffer.begin(); i != framebuffer.end() && !err; ++i)
	{
		const Slice &slice = i.slice();
		
		assert(slice.xSampling == 1 && slice.ySampling == 1);
		assert(_width == dw.max.x - dw.min.x + 1);
		assert(_height == dw.max.y - dw.min.y + 1);
		
		ChannelMap::const_iterator cache = _cache.find( i.name() );
	
		if( cache == _cache.end() )
		{
			// don't have this channel, fill with the fill value
			FillIterateData i_data(inter, slice, dw);
			
			err = suites.AEGPIterateSuite()->AEGP_IterateGeneric(_height, (void *)&i_data, FillIterate);
		}
		else
		{
			if(cache->second.bufH == NULL)
				throw NullExc("Why is the handle NULL?");
			
			char *buf = NULL;
			
			suites.MemorySuite()->AEGP_LockMemHandle(cache->second.bufH, (void**)&buf);
			
			if(buf == NULL)
				throw NullExc("Why is the locked handle NULL?");
			
			locked_handles.push_back(cache->second.bufH);
			
			
			CopyIterateData i_data(inter, buf, _width, cache->second.pix_type, slice, dw);
			
			err = suites.AEGPIterateSuite()->AEGP_IterateGeneric(_height, (void *)&i_data, CopyIterate);
		}
	}

	for(vector<AEIO_Handle>::const_iterator i = locked_handles.begin(); i != locked_handles.end(); ++i)
	{
		suites.MemorySuite()->AEGP_UnlockMemHandle(*i);
	}
	
	updateCacheTime();
	
	return err;
}


OpenEXR_ChannelCacheObjects::OpenEXR_ChannelCacheObjects(const SPBasicSuite *pica_basicP, const AEIO_InterruptFuncs *inter,
															Imf::HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership) :
	OpenEXR_ChannelCache(pica_basicP, inter, in, stream, tookOwnership, true),
	suites(pica_basicP)
{
	if(in == NULL || stream == NULL)
		throw NullExc("Unexpecedly NULL stream/file");
	
	const Box2i &dw = in->dataWindow();
	
#ifndef NDEBUG
	_width = dw.max.x - dw.min.x + 1;
	_height = dw.max.y - dw.min.y + 1;
#endif
	
	updateCacheTime();
}


A_Err
OpenEXR_ChannelCacheObjects::fillFrameBuffer(const AEIO_InterruptFuncs *inter, const Imf::FrameBuffer &framebuffer, const Imath::Box2i &dw)
{
	A_Err err = A_Err_NONE;
	
	updateCacheTime();
	
	try
	{
		_stream->softOpen();
	}
	catch(...)
	{
		assert(false);
	
		delete _infile;
		delete _stream;
		
		_stream = new IStreamPlatform(getPath().string());
		_infile = new HybridInputFile(*_stream);
	}
	
	try
	{
		_infile->setFrameBuffer(framebuffer);
		
	#ifndef NDEBUG
		assert(_width == dw.max.x - dw.min.x + 1);
		assert(_height == dw.max.y - dw.min.y + 1);
	#endif
		
		if(_infile->isTiled())
		{
			const int begin_row = 0;
			const int end_row = (_infile->numYTiles() - 1);

			const int tiled_row_blocksize = TiledRowBlockSize(*_infile);

			int y = begin_row;

			while(y <= end_row && PROG(y - begin_row, 1 + end_row - begin_row) )
			{
				int high_row = min(y + tiled_row_blocksize - 1, end_row);
				
				_infile->readTileRows(y, high_row);
				
				y = high_row + 1;
			}
		}
		else
		{
			const int scanline_block_size = ScanlineBlockSize(*_infile);
		
			int y = dw.min.y;
			
			while(y <= dw.max.y && PROG(y - dw.min.y, 1 + dw.max.y - dw.min.y) )
			{
				int high_scanline = min(y + scanline_block_size - 1, dw.max.y);
				
				_infile->readPixels(y, high_scanline);
				
				y = high_scanline + 1;
			}
		}
		
		if(err)
			throw CancelExc(err);
	}
	catch(IoExc) {} // we catch these so that partial files are read partially without error
	catch(InputExc) {}
	catch(...)
	{
		_stream->softClose();
		
		updateCacheTime();
		
		throw; // re-throw the exception
	}
	
	_stream->softClose();

	updateCacheTime();

	return err;
}


OpenEXR_CachePool::OpenEXR_CachePool() :
	_max_caches(0),
	_strategy(Strategy_MultiPart_Objects),
	_pica_basicP(NULL),
	_headerCachesInUse(0),
	_channelCachesInUse(0)
{

}


OpenEXR_CachePool::~OpenEXR_CachePool()
{
	configurePool(0);
}


static bool
compare_age(const OpenEXR_HeaderCache *first, const OpenEXR_HeaderCache *second)
{
	return first->cacheAge() > second->cacheAge();
}


void
OpenEXR_CachePool::configurePool(int max_caches, CacheStrategy strategy, const SPBasicSuite *pica_basicP)
{
	Lock lock(_mutex);

	assert(_headerCachesInUse == 0);
	assert(_channelCachesInUse == 0);
	
	_max_caches = max_caches;
	
	if(strategy != Strategy_Default)
		_strategy = strategy;
	
	if(pica_basicP)
		_pica_basicP = pica_basicP;
	
	
	_headerPool.sort(compare_age);

	while(_headerPool.size() > _max_caches)
	{
		delete _headerPool.front();
		
		_headerPool.pop_front();
	}
	
	
	_channelPool.sort(compare_age);

	while(_channelPool.size() > _max_caches)
	{
		delete _channelPool.front();
		
		_channelPool.pop_front();
	}
}


static bool
MatchDateTime(const DateTime &d1, const DateTime &d2)
{
#ifdef __APPLE__
	return (d1.fraction == d2.fraction &&
			d1.lowSeconds == d2.lowSeconds &&
			d1.highSeconds == d2.highSeconds);
#else
	return (d1.dwHighDateTime == d2.dwHighDateTime &&
			d1.dwLowDateTime == d2.dwLowDateTime);
#endif
}


OpenEXR_HeaderCache *
OpenEXR_CachePool::findHeaderCache(const IStreamPlatform *stream)
{
	Lock lock(_mutex);

	for(list<OpenEXR_HeaderCache *>::const_iterator i = _headerPool.begin(); i != _headerPool.end(); ++i)
	{
		OpenEXR_HeaderCache *cache = *i;
		
		if( MatchDateTime(stream->getModTime(), cache->getModTime()) &&
			stream->getPath() == cache->getPath() )
		{
			cache->updateCacheTime();
			
			assert(_headerCachesInUse == 0);
			
			_headerCachesInUse++;
			
			return cache;
		}
	}
	
	return NULL;
}


OpenEXR_HeaderCache *
OpenEXR_CachePool::addHeaderCache(HybridInputFile *in, IStreamPlatform *stream, bool &tookOwnership)
{
	list<OpenEXR_HeaderCache *> headersToDelete;

	{
		Lock lock(_mutex);

		_headerPool.sort(compare_age);
		
		assert(_headerCachesInUse == 0);
		
		while(_headerPool.size() && _headerPool.size() >= _max_caches)
		{
			headersToDelete.push_back( _headerPool.front() );
			
			_headerPool.pop_front();
		}
	}
	
	for(list<OpenEXR_HeaderCache *>::const_iterator i = headersToDelete.begin(); i != headersToDelete.end(); ++i)
		delete *i;
	
	
	if(_max_caches > 0)
	{
		try
		{
			OpenEXR_HeaderCache *new_cache = new OpenEXR_HeaderCache(in, stream, tookOwnership, true);
			
			{
				Lock lock(_mutex);
				
				_headerPool.push_back(new_cache);
				
				assert(_headerCachesInUse == 0);
				
				_headerCachesInUse++;
			}
			
			return new_cache;
		}
		catch(CancelExc) { throw; }
		catch(...) {}
	}
	
	return NULL;
}


OpenEXR_ChannelCache *
OpenEXR_CachePool::findChannelCache(const IStreamPlatform *stream, const AEIO_InterruptFuncs *inter)
{
	{
		Lock lock(_mutex);

		for(list<OpenEXR_ChannelCache *>::const_iterator i = _channelPool.begin(); i != _channelPool.end(); ++i)
		{
			OpenEXR_ChannelCache *cache = *i;
			
			if( MatchDateTime(stream->getModTime(), cache->getModTime()) &&
				stream->getPath() == cache->getPath() )
			{
				cache->updateCacheTime();
				
				assert(_channelCachesInUse == 0);
				
				_channelCachesInUse++;
				
				return cache;
			}
		}
	}
	
	// no ChannelCache found, look in HeaderCaches for objects?
	IStreamPlatform *cacheStream = NULL;
	HybridInputFile *cacheFile = NULL;
	
	{
		Lock lock(_mutex);
		
		for(list<OpenEXR_HeaderCache *>::const_iterator i = _headerPool.begin(); i != _headerPool.end(); ++i)
		{
			OpenEXR_HeaderCache *cache = *i;
			
			if( MatchDateTime(stream->getModTime(), cache->getModTime()) &&
				stream->getPath() == cache->getPath() )
			{
				cache->takeObjects(cacheFile, cacheStream);
				
				break;
			}
		}
	}
	
	if(cacheStream != NULL && cacheFile != NULL)
	{
		try
		{
			cacheStream->softOpen();
		}
		catch(...)
		{
			assert(false);
		
			delete cacheFile;
			delete cacheStream;
			
			return NULL;
		}

		bool tookOwnership = false;

		OpenEXR_ChannelCache *new_cache = NULL;

		try
		{
			unsigned int numChannels = 0;
			
			for(ChannelList::ConstIterator i = cacheFile->channels().begin(); i != cacheFile->channels().end() && numChannels <= 4; ++i)
				numChannels++;
			
			
			if(numChannels > 4 && (_strategy == Strategy_All_Memory || cacheFile->parts() == 1))
				new_cache = new OpenEXR_ChannelCacheMemory(_pica_basicP, inter, cacheFile, cacheStream, tookOwnership);
			else
				new_cache = new OpenEXR_ChannelCacheObjects(_pica_basicP, inter, cacheFile, cacheStream, tookOwnership);
		}
		catch(CancelExc)
		{
			assert(!tookOwnership);
			
			delete cacheFile;
			delete cacheStream;
			
			throw;
		}
		catch(...)
		{
			assert(!tookOwnership);
		
			delete cacheFile;
			delete cacheStream;
			
			return NULL;
		}

		if(!tookOwnership)
		{
			delete cacheFile;
			delete cacheStream;
		}

		{
			Lock lock(_mutex);
			
			_channelPool.push_back(new_cache);

			_channelCachesInUse++;
		}

		return new_cache;
	}
	
	return NULL;
}


OpenEXR_ChannelCache *
OpenEXR_CachePool::addChannelCache(HybridInputFile *in, IStreamPlatform *stream, const AEIO_InterruptFuncs *inter, bool &tookOwnership)
{
	list<OpenEXR_ChannelCache *> cachesToDelete;

	{
		Lock lock(_mutex);

		_channelPool.sort(compare_age);
		
		assert(_channelCachesInUse == 0);
		
		while(_channelPool.size() && _channelPool.size() >= _max_caches)
		{
			cachesToDelete.push_back( _channelPool.front() );
			
			_channelPool.pop_front();
		}
	}
	
	for(list<OpenEXR_ChannelCache *>::const_iterator i = cachesToDelete.begin(); i != cachesToDelete.end(); ++i)
		delete *i;
	

	if(_max_caches > 0)
	{
		try
		{
			unsigned int numChannels = 0;
			
			for(ChannelList::ConstIterator i = in->channels().begin(); i != in->channels().end() && numChannels <= 4; ++i)
				numChannels++;
			
			
			OpenEXR_ChannelCache *new_cache = NULL;
			
			if(numChannels > 4 && (_strategy == Strategy_All_Memory || in->parts() == 1))
				new_cache = new OpenEXR_ChannelCacheMemory(_pica_basicP, inter, in, stream, tookOwnership);
			else
				new_cache = new OpenEXR_ChannelCacheObjects(_pica_basicP, inter, in, stream, tookOwnership);
			
			{
				Lock lock(_mutex);
				
				_channelPool.push_back(new_cache);
				
				assert(_channelCachesInUse == 0);

				_channelCachesInUse++;
			}

			return new_cache;
		}
		catch(CancelExc) { throw; }
		catch(...) {}
	}
	
	return NULL;
}


void
OpenEXR_CachePool::returnHeaderCache(OpenEXR_HeaderCache *cache)
{
	Lock lock(_mutex);
	
	assert(cache != NULL);
	assert(_headerCachesInUse == 1);
	
	_headerCachesInUse--;
}


void
OpenEXR_CachePool::returnChannelCache(OpenEXR_ChannelCache *cache)
{
	Lock lock(_mutex);
	
	assert(cache != NULL);
	assert(_channelCachesInUse == 1);

	_channelCachesInUse--;
}


bool
OpenEXR_CachePool::deleteStaleCaches(int timeout)
{
	bool deleted_something = false;
	
	OpenEXR_HeaderCache *headerToDelete = NULL;
	OpenEXR_ChannelCache *cacheToDelete = NULL;
	
	{
		Lock lock(_mutex);
		
		if(_headerPool.size() > 0 && _headerCachesInUse == 0)
		{
			const int old_size = _headerPool.size();
		
			_headerPool.sort(compare_age);
			
			if( _headerPool.front()->cacheIsStale(timeout) )
			{
				headerToDelete = _headerPool.front(); // just going to delete one cache per cycle, the oldest one
				
				_headerPool.pop_front();
			}
			
			if(_headerPool.size() < old_size) // did something actually get deleted?
				deleted_something = true;
		}
		
		if(_channelPool.size() > 0 && _channelCachesInUse == 0)
		{
			const int old_size = _channelPool.size();
		
			_channelPool.sort(compare_age);
			
			if( _channelPool.front()->cacheIsStale(timeout) )
			{
				cacheToDelete = _channelPool.front(); // just going to delete one cache per cycle, the oldest one
				
				_channelPool.pop_front();
			}
			
			if(_channelPool.size() < old_size) // did something actually get deleted?
				deleted_something = true;
		}
	}
	
	delete headerToDelete;
	delete cacheToDelete;
	
	return deleted_something;
}


int ScanlineBlockSize(const Imf::HybridInputFile &in)
{
	// When multithreaded, we can see speedups if we read in enough scanlines at a time.
	// Piz and B44(A) compression use blocks of 32 scan lines,
	// so we'll make sure each thread gets at least a full block.
	
	const Header &head = in.header(0);
	
	int scanline_block_size = 32;
	
	if(head.compression() == DWAB_COMPRESSION)
		scanline_block_size = 256; // well, DWAB uses 256
	
	
	scanline_block_size *= max(globalThreadCount(), 1);
	
	
	if( head.hasTileDescription() )
	{
		const TileDescription &tiles = head.tileDescription();
		
		// if the image is tiled, use a multiple of tile height
		const int num_tiles = ceil((float)scanline_block_size / (float)tiles.ySize);
		
		scanline_block_size = num_tiles * tiles.ySize;
	}
	
			
	const Box2i &dataW = head.dataWindow();
	const Box2i &dispW = head.displayWindow();
	
	if(dataW != dispW)
		scanline_block_size *= 2; // might not be on tile/block boundries, so double up
	
				
	return scanline_block_size;
}


int TiledRowBlockSize(const Imf::HybridInputFile &in)
{
	if( in.isTiled() )
	{
		return ceil((float)max(globalThreadCount(), 1) / (float)in.numXTiles());
	}
	else
	{
		assert(false);
		
		return 1;
	}
}

